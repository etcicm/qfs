
**Clarifying questions on data in current database**

#. In 2007 data are sometimes reported with AggregationPeriod types other than Annual, usually without parallel reporting of annual data. Is this due to actual differences in methodology? Can the seasonal data be regarded as representing annual data, or if not, do annual data exist?

#. Some time series end early, some are late, and there seems to be different systems for station coding (WaterbaseID). Are you aware of cases where the same station has been assigned different WaterbaseIDs, e.g. due to changes in station codes or station names in past deliveries? In case, please supply a list of such cases, so that data from the same station can be combined into one time series.

#. There is generally some ambiguity regarding the determinands Ammonium vs Total ammonium in the database. You have mainly delivered Total ammonium data, but for 2009-2012 both types of data are delivered (not for the same station/year, it seems). Do these data actually represent different methodology? In case, explain. If not - have you reported NH4+ or the sum of NH4+ and NH3? 

#. The nitrate data for 2001 generally much lower than in all other years. Can these values be checked?

**Improving coverage of determinands, temporal and spatial coverage**

#. There are no data before 2000, and there are many short series and gaps in the longer series. Moreover, some determinants are missing in many of the years (e.g. total ammonium in 2008) and some determinands are reported for fewer stations in some years (e.g. orthophosphates). So - can data be redelivered for stations and determinands where data are missing over the period 1992-present? This is of particular interest for BOD5, ammonium, nitrate, total nitrogen, orthophosphates, total phosphorus. In practice, station time series that start late, end early or have long gaps cannot be included in the trend analysis for a given determinand.
   
#. Are the stations representative with respect to geographical and pressure gradients? If not, in which way can they be believed to be biased, and can data be delivered for more stations to reduce the bias?

#. WaterBodyID is missing for some stations - can this information be redelivered? And are all WaterBodyIDs delivered identical to the EUSurfaceWaterBodyCode assigned to water bodies in the WFD reporting? If not, can correct WaterBodyIDs be redelivered? Also, RBD code is missing for some stations.

**Links/references**

Please provide further references to

#. National water quality reports.

#. National water quality indicators.

#. National water quality data sets or databases.