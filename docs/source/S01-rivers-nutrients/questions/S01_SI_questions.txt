
**Clarifying questions on data in current database**

#. Some time series end in 2005 while others start in 2006. There is also an example for WaterbaseIDs SI_RV_2200 and SI_RV_2199 where one time series enda where the other starts, and these stations also have similar names. So - are you aware of cases where the same station has been assigned different WaterbaseIDs, e.g. due to changes in station codes or station names in past deliveries? In case, please supply a list of such cases, so that data from the same station can be combined into one time series.

#. There is generally some ambiguity regarding the determinands Ammonium vs Total ammonium in the database. You have delivered Total ammonium data until 2007, but Ammonium data from 2008. Is this due to differences in methodology? In case, please explain. If not - have you reported NH4+ or the sum of NH4+ and NH3?  

**Improving coverage of determinands, temporal and spatial coverage**

#. Some times series end early, start late and/or have gaps. Moreover, some determinands are missing in some years (e.g. total nitrogen, total organic carbon). So - can data be redelivered for stations and determinands where data are missing in the period 1992-present? This is of particular interest for BOD5, total ammonium, nitrate, total nitrogen, orthophosphates, total phosphorus. In practice, station time series that start late, end early or have long gaps cannot be included in the trend analysis for a given determinand.
   
#. Are the stations representative with respect to geographical and pressure gradients? If not, in which way can they be believed to be biased, and can data be delivered for more stations to reduce the bias?

#. WaterBodyID is missing for some stations - can this information be redelivered? And are all WaterBodyIDs delivered identical to the EUSurfaceWaterBodyCode assigned to water bodies in the WFD reporting? If not, can correct WaterBodyIDs be redelivered?

**Links/references**

Please provide further references to

#. National water quality reports.

#. National water quality indicators.

#. National water quality data sets or databases.