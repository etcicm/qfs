
**Clarifying questions on data in current database**

#. Was there a change in COD methodology in 2008, from mainly measuring CODMn to only measuring CODCr? I.e. is the reporting of the two different determinands correct?

#. Are you aware of cases where the same station has been assigned different WaterbaseIDs, e.g. due to changes in station codes or station names in past deliveries? In case, please supply a list of such cases, so that data from the same station can be combined into one time series.

#. There is generally some ambiguity regarding the determinands Ammonium vs Total ammonium in the database. You have mainly reported Total ammonium, but in 2010-2012 data were reported as Ammonium. Do these data actually represent different methodology? In case, explain. If not - have you reported NH4+ or the sum of NH4+ and NH3? 

#. For many stations the orthophosphate concentrations are very high in 2006 and/or 2007. Are these data correct? There are also particularly many high nitrate values in 1992. Are these data correct? Has there been a change in methodology?

**Improving coverage of determinands, temporal and spatial coverage**

#. Many time series start later than 1992, and there are also gaps in quite a few of the time series and some longer time series ending early (2006-2008). Moreover, many determinands are only reported in a few years (e.g. total phosphorus), and there are also single years without reporting (e.g. BOD5 in 2006). So - can data be redelivered for stations and determinands where data are missing over the period 1992-present? This is of particular interest for BOD5, ammonium, nitrate, total nitrogen, orthophosphates and total phosphorus. In practice, station time series that start late, end early or have long gaps cannot be included in the trend analysis for a given determinand.
   
#. Are the stations representative with respect to geographical and pressure gradients? If not, in which way can they be believed to be biased, and can data be delivered for more stations to reduce the bias?

#. WaterBodyID is missing for a number of stations - can this information be redelivered? And are all WaterBodyIDs delivered identical to the EUSurfaceWaterBodyCode assigned to water bodies in the WFD reporting? If not, can correct WaterBodyIDs be redelivered?

**Links/references**

Please provide further references to

#. National water quality reports.

#. National water quality indicators.

#. National water quality data sets or databases.
