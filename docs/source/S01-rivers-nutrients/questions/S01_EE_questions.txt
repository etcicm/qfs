
**Clarifying questions on data in current database**

#. Many time series end in 2010 while many other start in 2011. This seems to be due to a change in station coding rather than an actual replacement of some stations with new ones, as the stations in the two sets frequently have the same names and coordinates. The two sets also have distinctly different WaterbaseIDs. If it is correct that the same station has been assigned different WaterbaseIDs, can the data be redelivered with correct WaterbaseIDs, to prevent breaking of the time series?

#. There is generally some ambiguity regarding the determinands Ammonium vs Total ammonium in the database. You have usually delivered Total ammonium data, but in 2010-2012 data are delivered as Ammonium. Do these data actually represent different methodology? In case, explain. If not - have you reported NH4+ or the sum of NH4+ and NH3? 

#. In 2010 and 2011 BOD5 data are delivered, while BOD7 is delivered in the other years. Is this due to a change in methodology, or can it be a reporting error? Likewise CODMn is delivered in 2010-2012, while all data before that are CODCr. Is this due to an actual change in methodology or not?

**Improving coverage of determinands, temporal and spatial coverage**

#. There are gaps in some of the time series, and some time series start late. Moreover, data for some determinands are missing for some stations in some years. So - can data be redelivered for stations and determinands where data are missing over the period 1992-present? This is of particular interest for BOD, ammonium, nitrate, total nitrogen, orthophosphates and total phosphorus. In practice, station time series that start late, end early or have long gaps cannot be included in the trend analysis for a given determinand.
   
#. Are the stations representative with respect to geographical and pressure gradients? If not, in which way can they be believed to be biased, and can data be delivered for more stations to reduce the bias? E.g. there are hardly any data from the RBD EE3.

#. Are all WaterBodyIDs delivered identical to the EUSurfaceWaterBodyCode assigned to water bodies in the WFD reporting? If not, can correct WaterBodyIDs be redelivered?

**Links/references**

Please provide further references to

#. National water quality reports.

#. National water quality indicators.

#. National water quality data sets or databases.
