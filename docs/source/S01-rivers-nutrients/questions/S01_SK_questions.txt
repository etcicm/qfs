
**Clarifying questions on data in current database**

#. In 2002 and 2004 chlorophyll a and orthophosphates are sometimes only reported with AggregationPeriod Summer. Is this due to differences in methodology, or only in the reporting? Can the summer data from these years be regarded as representing annual data, or if not, do annual data exist? 

#. Some time series end in 2006 while others start in 2007. Are you aware of cases where the same station has been assigned different WaterbaseIDs, e.g. due to changes in station codes or station names in past deliveries? In case, please supply a list of such cases, so that data from the same station can be combined into one time series.

#. There are only some dissolved organic carbon data reported (for 2010 and 2012). Is it correct that another methodology was used in these instances? Usually total organic carbon is reported. 

#. There is generally some ambiguity regarding the determinands Ammonium vs Total ammonium in the database. You have sometimes delivered Total ammonium data, sometimes Ammonium and sometimes both. Is this reporting correct? How do you define the two determinands?

**Improving coverage of determinands, temporal and spatial coverage**

#. Many times series end early, start late and/or have gaps. Moreover, some determinands are missing in many years (e.g. ammonium/total ammonium) or reported for far fewer stations (e.g. total nitrogen, orthophosphates). So - can data be redelivered for stations and determinands where data are missing in the period 1992-present? This is of particular interest for BOD5, total ammonium/ammonium, nitrate, total nitrogen, orthophosphates and total phosphorus. In practice, station time series that start late, end early or have long gaps cannot be included in the trend analysis for a given determinand.
   
#. Are the stations representative with respect to geographical and pressure gradients? If not, in which way can they be believed to be biased, and can data be delivered for more stations to reduce the bias?

#. WaterBodyID is missing for some stations - can this information be redelivered? And are all WaterBodyIDs delivered identical to the EUSurfaceWaterBodyCode assigned to water bodies in the WFD reporting? If not, can correct WaterBodyIDs be redelivered?

**Links/references**

Please provide further references to

#. National water quality reports.

#. National water quality indicators.

#. National water quality data sets or databases.