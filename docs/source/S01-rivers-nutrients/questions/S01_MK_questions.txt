
**Clarifying questions on data in current database**

#. For WaterbaseID MK_RV_60909 certain determinands are reported only with AggregationPeriod Winter in 1999 and 2002. Is this due to an actual difference in methodology, or only in the reporting? Can the Winter data be regarded as representing annual data, or if not, do annual data exist?

#. There is generally some ambiguity regarding the determinands Ammonium vs Total ammonium in the database. You have delivered Total ammonium data. Do you define this as NH4+ or the sum of NH4+ and NH3? 

**Improving coverage of determinands, temporal and spatial coverage**

#. Many data series start in 1999 only, and there are many gaps in the time series. Moreover, some determinands are missing before 1999 (e.g. orthophosphates), some determinands are delivered for a few years only (e.g. total phosphorus) and there are single years without data for some determinands (e.g. total ammonium in 2007, BOD5 in 2009). So - can data be redelivered for stations and determinands where data are missing over the period 1992-present? This is of particular interest for BOD5, total ammonium, nitrate, orthophosphates and total phosphorus. In practice, station time series that start late, end early or have long gaps cannot be included in the trend analysis for a given determinand.
   
#. Are the stations representative with respect to geographical and pressure gradients? If not, in which way can they be believed to be biased, and can data be delivered for more stations to reduce the bias?

**Links/references**

Please provide further references to

#. National water quality reports.

#. National water quality indicators.

#. National water quality data sets or databases.