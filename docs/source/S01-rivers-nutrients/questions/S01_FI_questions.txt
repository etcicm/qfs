
**Clarifying questions on data in current database**

#. Many time series end in 2005, while there are some that start in 2006. Are you aware of cases where the same station has been assigned different WaterbaseIDs, e.g. due to changes in station codes or station names in past deliveries (possibly between 2005 and 2006)? In case, please supply a list of such cases, so that data from the same station can be combined into one time series.

#. There is generally some ambiguity regarding the determinands Ammonium vs Total ammonium in the database. You have delivered Total ammonium data. Do you define this as NH4+ or the sum of NH4+ and NH3? 

#. Total oxidised nitrogen is delivered for far more stations than nitrate. Is it correct that nitrate has not been analysed, or can nitrate data be delivered?

**Improving coverage of determinands, temporal and spatial coverage**

#. Many time series end in 2005, some that start in 2006, and there are other types of gaps in the time series as well. Morever, some determinands are missing for several stations and years (e.g. BOD7, nitrate). So - can data be redelivered for stations and determinands where data are missing over the period 1992-present? This is of particular interest for BOD7, total ammonium, nitrate, total oxidised nitrogen, total nitrogen, orthophosphates, total phosphorus. In practice, station time series that start late, end early or have long gaps cannot be included in the trend analysis for a given determinand.
   
#. Are the stations representative with respect to geographical and pressure gradients? If not, in which way can they be believed to be biased, and can data be delivered for more stations to reduce the bias?

#. WaterBodyID is missing for a number of stations, and where delivered it does not seem to be identical to the EUSurfaceWaterBodyCode assigned to water bodies in the WFD reporting? Can correct WaterBodyIDs be redelivered?

**Links/references**

Please provide further references to

#. National water quality reports.

#. National water quality indicators.

#. National water quality data sets or databases.
