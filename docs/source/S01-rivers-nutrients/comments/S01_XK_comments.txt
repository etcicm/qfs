Rivers - Nutrients, Organic Matter and General Physico-Chemical Determinands
============================================================================

About table 2-1
---------------

.. start-text1-placeholder

Kosovo started reporting WISE SoE data in 2008 and reported Annual data for the whole reporting period from 2008 to 2012,

Kosovo has reported data for total of 5 priority nutrients from 2008 to 2012, 
with BOD5, Nitrate, Orthophosphates, Total ammonium and Total phosphorus covering the whole period.

In the dataset there is no data for CODCr/CODMn.

.. end-text1-placeholder


About table 2-2
---------------

.. start-text2-placeholder

Kosovo has reported water quality data from 2008 to 2012 for 48 distinct river stations,
there were 45 stations in the 1st year and 42 in the latest.

There are no RBDs defined in Albania since it is not an EU member state.

There are no major problems with the stations in Kosovo.

.. end-text2-placeholder


About time series
-----------------

.. start-text3-placeholder

There are 41 stations with continuous time series (no gaps) covering the whole reporting period from 2008 to 2012.

.. end-text3-placeholder