Rivers - Nutrients, Organic Matter and General Physico-Chemical Determinands
============================================================================

About table 2-1
---------------

.. start-text1-placeholder

Austria reported Annual data for the whole reporting period from 1992 to 2012.

Austria has reported data for total of 9 priority nutrients from 1992 to 2012, 
3 of these determinands are covering the whole period: BOD5,Nitrate,Orthophosphates.

In the dataset there are data for all highest priority determinands.

.. end-text1-placeholder


About table 2-2
---------------

.. start-text2-placeholder

Austria has reported water quality data from 1992 to 2012 for 290 distinct river stations,
in the first year there were 98 river stations and then the number of stations was increasing up to 2006,
major increases were in 1993 (98->145), 2004 (148->287),
major decreases were in 2007 (280->65) and then the number was varying a bit to 60 stations in 2012.

There are 3 RBDs in Austria, for AT5000 with only one station and data only for the years 2004-2006.

There are 5 stations where coordinates fall outside the respective country boundary.

.. end-text2-placeholder


About time series
-----------------

.. start-text3-placeholder

29 of Austrian river stations have continuous time series (without gaps) covering the whole period from 1992 to 2012 
and additional 6 covering the period from 1993 to 2012.

4 of Austrian river stations have broken time series from 1992 to 2012 (1 year missing), 
59 have continuous time series from 1992 to 2006 and 119 of the Austrian river stations have continuous time series from 2004 to 2006.

.. end-text3-placeholder