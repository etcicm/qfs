Rivers - Nutrients, Organic Matter and General Physico-Chemical Determinands
============================================================================

About table 2-1
---------------

.. start-text1-placeholder

Poland reported Annual data for the whole reporting period, from 1992 to 2012,
additionally there is data for Spring, Summer, Autumn and Winter for shorter periods.

Poland has reported data for total of 10 priority nutrients from 1992 to 2012, 
with Nitrate and Total phosphorus covering the whole period.

In the dataset there is data for all highest priority nutrients.

.. end-text1-placeholder


About table 2-2
---------------

.. start-text2-placeholder

Poland has reported water quality data from 1992 to 2012 for 967 distinct river stations,
the number of stations increased from 101 in 1992 to 736 in 2012.

There are 9 official RBDs in Poland, with 6 covering whole reporting period, 1 was introduced in 2011
and another 2 in 2012.

There are no station issues in Poland.

.. end-text2-placeholder


About time series
-----------------

.. start-text3-placeholder

There are 4 stations with continuous time series (no gaps) covering the whole reporting period from 1992 to 2012
and additional 31 stations with continuous time series  longer than 10 years.

.. end-text3-placeholder