Rivers - Nutrients, Organic Matter and General Physico-Chemical Determinands
============================================================================

About table 2-1
---------------

.. start-text1-placeholder

Bosnia and Herzegovina reported Annual data for the whole reporting period from 2000 to 2012,
additional data for the Autumn period have been reported for 2010, for the GrowingSeason period from 2008 to 2011
and for the Summer period from 2001 to 2007.

Bosnia and Herzegovina has reported data for total of 8 priority nutrients from 2000 to 2012, 
5 of these determinand are covering the whole period: BOD5,CODMn,Nitrate,Total ammonium,Total phosphorus.


.. end-text1-placeholder


About table 2-2
---------------

.. start-text2-placeholder

Bosnia and Herzegovina has reported water quality data from 2000 to 2012 for 83 distinct river stations,
in the first year there were 28 river stations and then the number of stations was increasing up to 46 in 2009,
in 2010 there was a slight drop to 40 stations, in 2011 there was another drop to 33 stations
and in 2012 was again a drop to 30 stations.

There are no RBDs defined in Albania since it is not an EU member state.

There are 34 stations which have same coordinates (it might indicate an error in coordinates).

.. end-text2-placeholder


About time series
-----------------

.. start-text3-placeholder

5 of Bosnia and Herzegovina river stations have continuous time series (without gaps) covering the whole period from 2000 to 2012 
and additional 6 covering the period from 2000 to 2011, 
additional 10 covering the period from 2000 to 2010,
and 35 of Bosnia and Herzegovina river stations have short continuous time series lasting 1 or 2 years.

.. end-text3-placeholder