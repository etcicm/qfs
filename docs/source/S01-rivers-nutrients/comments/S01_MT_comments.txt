Rivers - Nutrients, Organic Matter and General Physico-Chemical Determinands
============================================================================

About table 2-1
---------------

.. start-text1-placeholder

Malta is a special case, there are no permanent rivers or lakes on Malta and therefore is not reporting
WISE SoE Rivers data. However there was some hazardous substances reporting in WISE SoE 2013.

.. end-text1-placeholder


About table 2-2
---------------

.. start-text2-placeholder

no data

.. end-text2-placeholder


About time series
-----------------

.. start-text3-placeholder

no data

.. end-text3-placeholder