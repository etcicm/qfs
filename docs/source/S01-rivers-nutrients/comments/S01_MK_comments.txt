Rivers - Nutrients, Organic Matter and General Physico-Chemical Determinands
============================================================================

About table 2-1
---------------

.. start-text1-placeholder

Macedonia reported Annual data for the whole reporting period from 1992 to 2012, 
additionally there is data for Winter aggregation period from 1999 to 2002. There is 
a gap in reporting in years 1997 and 1998.

Macedonia has reported data for total of 8 priority nutrients from 1992 to 2012, 
with BOD5 and Total ammonium covering the whole period.

.. end-text1-placeholder


About table 2-2
---------------

.. start-text2-placeholder

Macedonia has reported water quality data from 1992 to 2012 for 20 distinct river stations,
it started with 9 stations in 1992 and up to 18 in 2012.

There are no RBDs defined in Macedonia since it is not an EU member state.

There are no major problems with the stations in Macedonia.

.. end-text2-placeholder


About time series
-----------------

.. start-text3-placeholder

In Macedonia there are no continuous time series (without gaps) covering the whole period from 1992 to 2012
since there was a gap in reporting in years 1997 and 1998.

The longest broken (with gaps) time series is 19 years long and has 5 stations.

.. end-text3-placeholder