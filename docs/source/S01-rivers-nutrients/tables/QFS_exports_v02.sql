### loop  @cc = {'AL','AT','BA','BE','BG','CH','CY','CZ','DE','DK','EE','ES','FI','FR','GB','GR','HR','HU','IE','IS','IT','LI','LT','LU','LV','ME','MK','MT','NL','NO','PL','PT','RO','RS','SE','SI','SK','TR','XK'}
SET @cc = 'HR';

### AggregationPeriod ###
select AggregationPeriod, 
	NULLIF(COUNT(IF(Year = 1992, 1, null)), 0) AS '1992',
	NULLIF(COUNT(IF(Year = 1993, 1, null)), 0) AS '1993',
	NULLIF(COUNT(IF(Year = 1994, 1, null)), 0) AS '1994',
	NULLIF(COUNT(IF(Year = 1995, 1, null)), 0) AS '1995',
	NULLIF(COUNT(IF(Year = 1996, 1, null)), 0) AS '1996',
	NULLIF(COUNT(IF(Year = 1997, 1, null)), 0) AS '1997',
	NULLIF(COUNT(IF(Year = 1998, 1, null)), 0) AS '1998',
	NULLIF(COUNT(IF(Year = 1999, 1, null)), 0) AS '1999',
	NULLIF(COUNT(IF(Year = 2000, 1, null)), 0) AS '2000',
	NULLIF(COUNT(IF(Year = 2001, 1, null)), 0) AS '2001',
	NULLIF(COUNT(IF(Year = 2002, 1, null)), 0) AS '2002',
	NULLIF(COUNT(IF(Year = 2003, 1, null)), 0) AS '2003',
	NULLIF(COUNT(IF(Year = 2004, 1, null)), 0) AS '2004',
	NULLIF(COUNT(IF(Year = 2005, 1, null)), 0) AS '2005',
	NULLIF(COUNT(IF(Year = 2006, 1, null)), 0) AS '2006',
	NULLIF(COUNT(IF(Year = 2007, 1, null)), 0) AS '2007',
	NULLIF(COUNT(IF(Year = 2008, 1, null)), 0) AS '2008',
	NULLIF(COUNT(IF(Year = 2009, 1, null)), 0) AS '2009',
	NULLIF(COUNT(IF(Year = 2010, 1, null)), 0) AS '2010',
	NULLIF(COUNT(IF(Year = 2011, 1, null)), 0) AS '2011',
	NULLIF(COUNT(IF(Year = 2012, 1, null)), 0) AS '2012',
	NULLIF(COUNT(IF(Year between 1992 and 2012, 1, null)), 0) AS 'Grand Total'

from data_nutrients

where 1
and CountryCode = @cc
and dataset_record = 1 and (RecordDelete is null or RecordDelete != 1)

GROUP BY AggregationPeriod

UNION

select 'Total' AS AggregationPeriod, 
	NULLIF(COUNT(IF(Year = 1992, 1, null)), 0) AS '1992',
	NULLIF(COUNT(IF(Year = 1993, 1, null)), 0) AS '1993',
	NULLIF(COUNT(IF(Year = 1994, 1, null)), 0) AS '1994',
	NULLIF(COUNT(IF(Year = 1995, 1, null)), 0) AS '1995',
	NULLIF(COUNT(IF(Year = 1996, 1, null)), 0) AS '1996',
	NULLIF(COUNT(IF(Year = 1997, 1, null)), 0) AS '1997',
	NULLIF(COUNT(IF(Year = 1998, 1, null)), 0) AS '1998',
	NULLIF(COUNT(IF(Year = 1999, 1, null)), 0) AS '1999',
	NULLIF(COUNT(IF(Year = 2000, 1, null)), 0) AS '2000',
	NULLIF(COUNT(IF(Year = 2001, 1, null)), 0) AS '2001',
	NULLIF(COUNT(IF(Year = 2002, 1, null)), 0) AS '2002',
	NULLIF(COUNT(IF(Year = 2003, 1, null)), 0) AS '2003',
	NULLIF(COUNT(IF(Year = 2004, 1, null)), 0) AS '2004',
	NULLIF(COUNT(IF(Year = 2005, 1, null)), 0) AS '2005',
	NULLIF(COUNT(IF(Year = 2006, 1, null)), 0) AS '2006',
	NULLIF(COUNT(IF(Year = 2007, 1, null)), 0) AS '2007',
	NULLIF(COUNT(IF(Year = 2008, 1, null)), 0) AS '2008',
	NULLIF(COUNT(IF(Year = 2009, 1, null)), 0) AS '2009',
	NULLIF(COUNT(IF(Year = 2010, 1, null)), 0) AS '2010',
	NULLIF(COUNT(IF(Year = 2011, 1, null)), 0) AS '2011',
	NULLIF(COUNT(IF(Year = 2012, 1, null)), 0) AS '2012',
	NULLIF(COUNT(IF(Year between 1992 and 2012, 1, null)), 0) AS 'Grand Total'

from data_nutrients

where 1
and CountryCode = @cc
and dataset_record = 1 and (RecordDelete is null or RecordDelete != 1);




### Determinand_Nutrients ###
select b.`Group` AS Determinand_Group, qfs_order, Determinand_Nutrients, 
	NULLIF(COUNT(IF(Year = 1992, 1, null)), 0) AS '1992',
	NULLIF(COUNT(IF(Year = 1993, 1, null)), 0) AS '1993',
	NULLIF(COUNT(IF(Year = 1994, 1, null)), 0) AS '1994',
	NULLIF(COUNT(IF(Year = 1995, 1, null)), 0) AS '1995',
	NULLIF(COUNT(IF(Year = 1996, 1, null)), 0) AS '1996',
	NULLIF(COUNT(IF(Year = 1997, 1, null)), 0) AS '1997',
	NULLIF(COUNT(IF(Year = 1998, 1, null)), 0) AS '1998',
	NULLIF(COUNT(IF(Year = 1999, 1, null)), 0) AS '1999',
	NULLIF(COUNT(IF(Year = 2000, 1, null)), 0) AS '2000',
	NULLIF(COUNT(IF(Year = 2001, 1, null)), 0) AS '2001',
	NULLIF(COUNT(IF(Year = 2002, 1, null)), 0) AS '2002',
	NULLIF(COUNT(IF(Year = 2003, 1, null)), 0) AS '2003',
	NULLIF(COUNT(IF(Year = 2004, 1, null)), 0) AS '2004',
	NULLIF(COUNT(IF(Year = 2005, 1, null)), 0) AS '2005',
	NULLIF(COUNT(IF(Year = 2006, 1, null)), 0) AS '2006',
	NULLIF(COUNT(IF(Year = 2007, 1, null)), 0) AS '2007',
	NULLIF(COUNT(IF(Year = 2008, 1, null)), 0) AS '2008',
	NULLIF(COUNT(IF(Year = 2009, 1, null)), 0) AS '2009',
	NULLIF(COUNT(IF(Year = 2010, 1, null)), 0) AS '2010',
	NULLIF(COUNT(IF(Year = 2011, 1, null)), 0) AS '2011',
	NULLIF(COUNT(IF(Year = 2012, 1, null)), 0) AS '2012',
	NULLIF(COUNT(IF(Year between 1992 and 2012, 1, null)), 0) AS 'Grand Total'

from data_nutrients as a
inner join waterbase_common.tb_fiche_determinand_order as b using(Determinand_Nutrients)

where 1
and CountryCode = @cc
and dataset_record = 1 and (RecordDelete is null or RecordDelete != 1)

and qfs_order is not null

GROUP BY Determinand_Nutrients

UNION

select 'Total' AS Determinand_Group, 99 AS qfs_order, NULL AS Determinand_Nutrients, 
	NULLIF(COUNT(IF(Year = 1992, 1, null)), 0) AS '1992',
	NULLIF(COUNT(IF(Year = 1993, 1, null)), 0) AS '1993',
	NULLIF(COUNT(IF(Year = 1994, 1, null)), 0) AS '1994',
	NULLIF(COUNT(IF(Year = 1995, 1, null)), 0) AS '1995',
	NULLIF(COUNT(IF(Year = 1996, 1, null)), 0) AS '1996',
	NULLIF(COUNT(IF(Year = 1997, 1, null)), 0) AS '1997',
	NULLIF(COUNT(IF(Year = 1998, 1, null)), 0) AS '1998',
	NULLIF(COUNT(IF(Year = 1999, 1, null)), 0) AS '1999',
	NULLIF(COUNT(IF(Year = 2000, 1, null)), 0) AS '2000',
	NULLIF(COUNT(IF(Year = 2001, 1, null)), 0) AS '2001',
	NULLIF(COUNT(IF(Year = 2002, 1, null)), 0) AS '2002',
	NULLIF(COUNT(IF(Year = 2003, 1, null)), 0) AS '2003',
	NULLIF(COUNT(IF(Year = 2004, 1, null)), 0) AS '2004',
	NULLIF(COUNT(IF(Year = 2005, 1, null)), 0) AS '2005',
	NULLIF(COUNT(IF(Year = 2006, 1, null)), 0) AS '2006',
	NULLIF(COUNT(IF(Year = 2007, 1, null)), 0) AS '2007',
	NULLIF(COUNT(IF(Year = 2008, 1, null)), 0) AS '2008',
	NULLIF(COUNT(IF(Year = 2009, 1, null)), 0) AS '2009',
	NULLIF(COUNT(IF(Year = 2010, 1, null)), 0) AS '2010',
	NULLIF(COUNT(IF(Year = 2011, 1, null)), 0) AS '2011',
	NULLIF(COUNT(IF(Year = 2012, 1, null)), 0) AS '2012',
	NULLIF(COUNT(IF(Year between 1992 and 2012, 1, null)), 0) AS 'Grand Total'

from data_nutrients
inner join waterbase_common.tb_fiche_determinand_order as b using(Determinand_Nutrients)

where 1
and CountryCode = @cc
and dataset_record = 1 and (RecordDelete is null or RecordDelete != 1)

ORDER BY qfs_order;

### Time Series ###
select s.WaterbaseId, s.NationalStationName, s.RiverName, s.RBDcode,
	NULLIF(COUNT(IF(Year = 1992, 1, null)), 0) AS '1992',
	NULLIF(COUNT(IF(Year = 1993, 1, null)), 0) AS '1993',
	NULLIF(COUNT(IF(Year = 1994, 1, null)), 0) AS '1994',
	NULLIF(COUNT(IF(Year = 1995, 1, null)), 0) AS '1995',
	NULLIF(COUNT(IF(Year = 1996, 1, null)), 0) AS '1996',
	NULLIF(COUNT(IF(Year = 1997, 1, null)), 0) AS '1997',
	NULLIF(COUNT(IF(Year = 1998, 1, null)), 0) AS '1998',
	NULLIF(COUNT(IF(Year = 1999, 1, null)), 0) AS '1999',
	NULLIF(COUNT(IF(Year = 2000, 1, null)), 0) AS '2000',
	NULLIF(COUNT(IF(Year = 2001, 1, null)), 0) AS '2001',
	NULLIF(COUNT(IF(Year = 2002, 1, null)), 0) AS '2002',
	NULLIF(COUNT(IF(Year = 2003, 1, null)), 0) AS '2003',
	NULLIF(COUNT(IF(Year = 2004, 1, null)), 0) AS '2004',
	NULLIF(COUNT(IF(Year = 2005, 1, null)), 0) AS '2005',
	NULLIF(COUNT(IF(Year = 2006, 1, null)), 0) AS '2006',
	NULLIF(COUNT(IF(Year = 2007, 1, null)), 0) AS '2007',
	NULLIF(COUNT(IF(Year = 2008, 1, null)), 0) AS '2008',
	NULLIF(COUNT(IF(Year = 2009, 1, null)), 0) AS '2009',
	NULLIF(COUNT(IF(Year = 2010, 1, null)), 0) AS '2010',
	NULLIF(COUNT(IF(Year = 2011, 1, null)), 0) AS '2011',
	NULLIF(COUNT(IF(Year = 2012, 1, null)), 0) AS '2012',
	NULLIF(COUNT(*), 0) AS 'Grand Total',
	NULLIF(COUNT(DISTINCT Year),0) AS 'No of years'

from data_stations as s
inner join data_nutrients as a using(CountryCode, NationalStationID)

where 1

and CountryCode = @cc
and Year between 1992 and 2012

and s.dataset_record = 1 and (s.RecordDelete is null or s.RecordDelete != 1)
and a.dataset_record = 1 and (a.RecordDelete is null or a.RecordDelete != 1)

GROUP BY CountryCode, NationalStationID

UNION

select 'Total' AS WaterbaseId, null as NationalStationName, null as RiverName, null as RBDcode, 
	NULLIF(COUNT(IF(Year = 1992, 1, null)), 0) AS '1992',
	NULLIF(COUNT(IF(Year = 1993, 1, null)), 0) AS '1993',
	NULLIF(COUNT(IF(Year = 1994, 1, null)), 0) AS '1994',
	NULLIF(COUNT(IF(Year = 1995, 1, null)), 0) AS '1995',
	NULLIF(COUNT(IF(Year = 1996, 1, null)), 0) AS '1996',
	NULLIF(COUNT(IF(Year = 1997, 1, null)), 0) AS '1997',
	NULLIF(COUNT(IF(Year = 1998, 1, null)), 0) AS '1998',
	NULLIF(COUNT(IF(Year = 1999, 1, null)), 0) AS '1999',
	NULLIF(COUNT(IF(Year = 2000, 1, null)), 0) AS '2000',
	NULLIF(COUNT(IF(Year = 2001, 1, null)), 0) AS '2001',
	NULLIF(COUNT(IF(Year = 2002, 1, null)), 0) AS '2002',
	NULLIF(COUNT(IF(Year = 2003, 1, null)), 0) AS '2003',
	NULLIF(COUNT(IF(Year = 2004, 1, null)), 0) AS '2004',
	NULLIF(COUNT(IF(Year = 2005, 1, null)), 0) AS '2005',
	NULLIF(COUNT(IF(Year = 2006, 1, null)), 0) AS '2006',
	NULLIF(COUNT(IF(Year = 2007, 1, null)), 0) AS '2007',
	NULLIF(COUNT(IF(Year = 2008, 1, null)), 0) AS '2008',
	NULLIF(COUNT(IF(Year = 2009, 1, null)), 0) AS '2009',
	NULLIF(COUNT(IF(Year = 2010, 1, null)), 0) AS '2010',
	NULLIF(COUNT(IF(Year = 2011, 1, null)), 0) AS '2011',
	NULLIF(COUNT(IF(Year = 2012, 1, null)), 0) AS '2012',
	NULLIF(COUNT(*), 0) AS 'Grand Total',
	NULLIF(COUNT(DISTINCT Year),0) AS 'No of years'

from data_stations as s
inner join data_nutrients as a using(CountryCode, NationalStationID)

where 1

and CountryCode = @cc
and Year between 1992 and 2012

and s.dataset_record = 1 and (s.RecordDelete is null or s.RecordDelete != 1)
and a.dataset_record = 1 and (a.RecordDelete is null or a.RecordDelete != 1)
;

### RBD ###
select 
	s.RBDcode, 
	NULLIF(COUNT(DISTINCT IF(Year = 1992, NationalStationID, null)), 0) AS '1992',
	NULLIF(COUNT(DISTINCT IF(Year = 1993, NationalStationID, null)), 0) AS '1993',
	NULLIF(COUNT(DISTINCT IF(Year = 1994, NationalStationID, null)), 0) AS '1994',
	NULLIF(COUNT(DISTINCT IF(Year = 1995, NationalStationID, null)), 0) AS '1995',
	NULLIF(COUNT(DISTINCT IF(Year = 1996, NationalStationID, null)), 0) AS '1996',
	NULLIF(COUNT(DISTINCT IF(Year = 1997, NationalStationID, null)), 0) AS '1997',
	NULLIF(COUNT(DISTINCT IF(Year = 1998, NationalStationID, null)), 0) AS '1998',
	NULLIF(COUNT(DISTINCT IF(Year = 1999, NationalStationID, null)), 0) AS '1999',
	NULLIF(COUNT(DISTINCT IF(Year = 2000, NationalStationID, null)), 0) AS '2000',
	NULLIF(COUNT(DISTINCT IF(Year = 2001, NationalStationID, null)), 0) AS '2001',
	NULLIF(COUNT(DISTINCT IF(Year = 2002, NationalStationID, null)), 0) AS '2002',
	NULLIF(COUNT(DISTINCT IF(Year = 2003, NationalStationID, null)), 0) AS '2003',
	NULLIF(COUNT(DISTINCT IF(Year = 2004, NationalStationID, null)), 0) AS '2004',
	NULLIF(COUNT(DISTINCT IF(Year = 2005, NationalStationID, null)), 0) AS '2005',
	NULLIF(COUNT(DISTINCT IF(Year = 2006, NationalStationID, null)), 0) AS '2006',
	NULLIF(COUNT(DISTINCT IF(Year = 2007, NationalStationID, null)), 0) AS '2007',
	NULLIF(COUNT(DISTINCT IF(Year = 2008, NationalStationID, null)), 0) AS '2008',
	NULLIF(COUNT(DISTINCT IF(Year = 2009, NationalStationID, null)), 0) AS '2009',
	NULLIF(COUNT(DISTINCT IF(Year = 2010, NationalStationID, null)), 0) AS '2010',
	NULLIF(COUNT(DISTINCT IF(Year = 2011, NationalStationID, null)), 0) AS '2011',
	NULLIF(COUNT(DISTINCT IF(Year = 2012, NationalStationID, null)), 0) AS '2012'

from data_stations as s
inner join data_nutrients as a using(CountryCode, NationalStationID)

where 1
and CountryCode = @cc
and s.dataset_record = 1 and (s.RecordDelete is null or s.RecordDelete != 1)

GROUP BY s.RBDcode

UNION 

select 
	'Total' AS RBDcode, 
	NULLIF(COUNT(DISTINCT IF(Year = 1992, NationalStationID, null)), 0) AS '1992',
	NULLIF(COUNT(DISTINCT IF(Year = 1993, NationalStationID, null)), 0) AS '1993',
	NULLIF(COUNT(DISTINCT IF(Year = 1994, NationalStationID, null)), 0) AS '1994',
	NULLIF(COUNT(DISTINCT IF(Year = 1995, NationalStationID, null)), 0) AS '1995',
	NULLIF(COUNT(DISTINCT IF(Year = 1996, NationalStationID, null)), 0) AS '1996',
	NULLIF(COUNT(DISTINCT IF(Year = 1997, NationalStationID, null)), 0) AS '1997',
	NULLIF(COUNT(DISTINCT IF(Year = 1998, NationalStationID, null)), 0) AS '1998',
	NULLIF(COUNT(DISTINCT IF(Year = 1999, NationalStationID, null)), 0) AS '1999',
	NULLIF(COUNT(DISTINCT IF(Year = 2000, NationalStationID, null)), 0) AS '2000',
	NULLIF(COUNT(DISTINCT IF(Year = 2001, NationalStationID, null)), 0) AS '2001',
	NULLIF(COUNT(DISTINCT IF(Year = 2002, NationalStationID, null)), 0) AS '2002',
	NULLIF(COUNT(DISTINCT IF(Year = 2003, NationalStationID, null)), 0) AS '2003',
	NULLIF(COUNT(DISTINCT IF(Year = 2004, NationalStationID, null)), 0) AS '2004',
	NULLIF(COUNT(DISTINCT IF(Year = 2005, NationalStationID, null)), 0) AS '2005',
	NULLIF(COUNT(DISTINCT IF(Year = 2006, NationalStationID, null)), 0) AS '2006',
	NULLIF(COUNT(DISTINCT IF(Year = 2007, NationalStationID, null)), 0) AS '2007',
	NULLIF(COUNT(DISTINCT IF(Year = 2008, NationalStationID, null)), 0) AS '2008',
	NULLIF(COUNT(DISTINCT IF(Year = 2009, NationalStationID, null)), 0) AS '2009',
	NULLIF(COUNT(DISTINCT IF(Year = 2010, NationalStationID, null)), 0) AS '2010',
	NULLIF(COUNT(DISTINCT IF(Year = 2011, NationalStationID, null)), 0) AS '2011',
	NULLIF(COUNT(DISTINCT IF(Year = 2012, NationalStationID, null)), 0) AS '2012'

from data_stations as s
inner join data_nutrients as a using(CountryCode, NationalStationID)

where 1
and CountryCode = @cc
and s.dataset_record = 1 and (s.RecordDelete is null or s.RecordDelete != 1)

;

### All data ###
select 
	s.CountryCode, s.WaterbaseId, s.RiverName, s.NationalStationName, 
	n.`Year`,
	n.Determinand_Nutrients, 
	n.Mean,
	n.AggregationPeriod,
	s.RBDcode,
	s.WaterBodyID,
	s.WaterBodyName,
	n.AggregationMonths

from data_stations as s
inner join data_nutrients as n using(CountryCode, NationalStationID)

where 1

and CountryCode = @cc

and s.dataset_record = 1 and (s.RecordDelete is null or s.RecordDelete != 1)
and n.dataset_record = 1 and (n.RecordDelete is null or n.RecordDelete != 1)
;

