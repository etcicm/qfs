
**Clarifying questions on data in current database**

#. Can EQR values be reported for both phytobenthos and invertebrates? If not, can normalised EQR values be reported?
#. How often are you able to report the same BQE for the same station: annually, every two years, every three years or only once every six years? Is this frequency different for phytobenthos than for macroinvertebrates?


**Improving coverage of determinands, temporal and spatial coverage**

#. Is it possible to increase the number of stations in the future reporting?
#. Can InvertebratesEQR and PhytobenthosEQR be reported from more river stations in the RBDs ES010 and ES016? 
#. Can the SoE stations be checked and adjusted to obtain consistency with the WFD status class distribution for the specific BQEs in future reporting (avoiding bias towards too many in high or good status or too many in moderate or worse status)?
#. If the biological determinands were expanded to include fish in rivers, would you be willing to report any SoE data for this BQE (EQR and/or Status class)?


**Links/references**

Please provide further references to

#. National water biology reports

#. National water biology indicators


