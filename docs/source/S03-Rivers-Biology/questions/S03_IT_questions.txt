
**Clarifying questions on data in current database**

#. Can Italy report river biology data in the format requested by EEA? If yes, please reply to the questions below:
#. How often are you able to report the same BQE for the same station: annually, every two years, every three years or only once every six years? Is this frequency different for phytobenthos than for macroinvertebrates?


**Improving coverage of determinands, temporal and spatial coverage**

#. Are data for both Phytobenthos and Macroinvertebrates available and can these be reported?
#. Is it possible to increase the number of stations in the future reporting?
#. Can data for the years 2010-2013 be delivered with the next data request in 2015? 
#. Can data for all RBDs be reported in future? 
#. Could the number of SoE stations be expanded to obtain consistency with WFD data concerning the distribution of status classes for the specific BQEs? This is important to avoid bias towards too many in high or good status or too many in moderate or worse status
#. If the biological determinands were expanded to include fish in rivers, would you be willing to report any SoE data for this BQE (EQR and/or Status class)?

**Links/references**

Please provide further references to

#. National water biology reports

#. National water biology indicators

