
**Clarifying questions on data in current database**

#. Can Denmark eihter report normalised EQRs or report the classification system to allow the ETC to normalise the EQR values for river invertebrates?
#. How often are you able to report the same BQE for the same station: annually, every two years, every three years or only once every six years? Is this frequency different for phytobenthos than for macroinvertebrates?


**Improving coverage of determinands, temporal and spatial coverage**

#. Are data for Phytobenthos available and can these be reported?
#. Is it possible to increase the number of stations in the future reporting?
#. Can data for the year 2012 be delivered with the next data request? 
#. Can data for missing RBDs be reported in future? (There is no reporting in RBD DK4. Are there any stations with monitoring in this RBD?) 
#. Could the number of SoE stations be expanded to obtain consistency with WFD data concerning the distribution of status classes for the specific BQEs? This is important to avoid bias towards too many in high or good status or too many in moderate or worse status
#. If the biological determinands were expanded to include fish in rivers, would you be willing to report any SoE data for this BQE (EQR and/or Status class)?

**Links/references**

Please provide further references to

#. National water biology reports

#. National water biology indicators


