
**Clarifying questions on data in current database**


#. No EQR values are reported. What is the reason and can EQR values be included in future reporting?

#. Can you please reply to ETC feedback questions concerning the classification system? See questions posted in CDR 10.03.2014: http://cdr.eionet.europa.eu/de/eea/ewn1/envumo1xa/feedback1394459564). 

#. How often are you able to report the same BQE for the same station: annually, every two years, every three years or only once every six years? Is this frequency different for phytobenthos than for macroinvertebrates?


**Improving coverage of determinands, temporal and spatial coverage**

#. Is it possible to increase the number of stations in the future reporting?
#. Can data for missing RBDs be reported in future? There is no reporting from RBDs DE1000, DE2000, DE3000, DE6000, DE7000, DE9610, DE9650. Are there any stations with monitoring in these RBDs? 
#. Could the SoE stations be expanded to obtain better consistency with the WFD status class distribution for the specific BQEs in future reporting (avoiding bias towards too many in high or good status or too many in moderate or worse status)?
#. If the biological determinands were expanded to include fish in rivers, would you be willing to report any SoE data for this BQE (EQR and/or Status class)?


**Links/references**

Please provide further references to

#. National water biology reports

#. National water biology indicators


