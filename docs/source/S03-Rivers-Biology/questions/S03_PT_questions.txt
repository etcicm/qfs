
**Clarifying questions on data in current database**

#. No EQR values are reported. What is the reason and can EQR values be reported in future reporting?
#. How often are you able to report the same BQE for the same station: annually, every two years, every three years or only once every six years? Is this frequency different for phytobenthos than for macroinvertebrates?


**Improving coverage of determinands, temporal and spatial coverage**

#. Is it possible to increase the number of stations in the future reporting?
#. Can data for 2012, 2013 and 2014 be delivered with the next data request in 2015? 
#. Can data for missing RBDs be reported in future? (There is no reporting in RBDs PTRH9 and PTRH10. Are there any stations with monitoring in these RBDs?) 
#. Could the number of SoE stations be expanded to obtain consistency with WFD data concerning the distribution of status classes for the specific BQEs? This is important to avoid bias towards too many in high or good status or too many in moderate or worse status
#. If the biological determinands were expanded to include fish in rivers, would you be willing to report any SoE data for this BQE (EQR and/or Status class)?

**Links/references**

Please provide further references to

#. National water biology reports

#. National water biology indicators


