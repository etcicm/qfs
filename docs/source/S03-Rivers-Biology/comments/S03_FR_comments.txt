
Country specific text about table S03, tables 26 and 27
-------------------------------------------


.. start-text1-placeholder

#. Data quantity issues:

	-	France has reported rivers biology data from the years 2010 and 2011 for both the requested biological determinands PhytobenthosEQR and InvertebrateEQR. 
	-	No data have been reported from 2012 for any of the two requested river biology determinands. 
	-	France reported river biology data in total from ca. 1000 stations for phytobenthos and ca. 800 stations for invertebrates.
	-	The total number of stations was stable in both years where the biological data have been reported.
	-	Data has been reported from all RBDs for both phytobenthos and invertebrates.  

#. Data quality issues: 

	-	For ca. 2% of the stations the mean value was missing for phytobenthosEQR 
	-	For ca. 2% of the stations the normalised EQR values and status class for both phytobenthos and invertebrates could not be calculated by the ETC, due to no matching classification system. 
	-	For ca. 2% of the stations the normalised EQR value for invertebrates was not available.

.. end-text1-placeholder
