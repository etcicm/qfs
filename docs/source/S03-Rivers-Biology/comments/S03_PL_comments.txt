
Country specific text about table S03, tables 26 and 27
-------------------------------------------


.. start-text1-placeholder

#. Data quantity issues:

	-	Poland has reported rivers biology data from 2010, 2011 and 2012 for the requested biological determinand PhytobenthosEQR and from 2010 and 2012 for InvertebrateEQR. 
	-	The total number of stations reported in 2012 was 130 for phytobenthosEQR and 158 for invertebratesEQR. For phytobenthos this is a large decrease from 2010, while for invertebrates this is a large increase from 2010. 
	-	Most of the data (more than 90 percent) were reported from two of the 10 national RBDs (PL2000 Vistula and PL6000 Oder). 

#. Data quality issues: 

	-	DeterminandStatusClass was incorrect for a large part of the reported data. DeterminandStatusClass and MeanValueNormEQR have been corrected by ETC.
	-	For PhytobenthosEQR data from 2010 and 2011, there was no matching classification system, so normalised EQR could not be calculated. 
	-	Aggregation period was reported as growing season or spring, summer, autumn, winter, and not as annual, as recommended by the ETC.
	-	Minor QA issues on spelling of status class and waterbody type IC were corrected by ETC. 

.. end-text1-placeholder
