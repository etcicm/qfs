
Country specific text about table S03, tables 26 and 27
-------------------------------------------


.. start-text1-placeholder

#. Data quantity issues:

	-	Bulgaria has reported rivers biology data from all three years 2010, 2011 and 2012 for the requested biological determinand InvertebrateEQR. 
	-	Data have been reported from 2011 and 2012 for the requested biological determinand PhytobenthosEQR. 
	-	Total number of river stations in 2012 was 38 for phytobenthos and 73 for invertebrates. 
	-	The number of stations was largely increased from 2011 to 2012 for phytobenthos, and was rather stable for all the three years of reporting for invertebrates. 
	-	Data are reported from all RBDs (BG1000, BG2000, BG3000, BG4000) for invertebrates, and for all RBDs except BG2000 for phytobenthos. Most of the stations reported was from two RBDs (BG1000 and BG3000). 

#. Data quality issues: 

	-	For most stations, the data reported were seasonal (spring, summer, autumn) rather than annual (which is recommended). 
	-	The reported mean values for invertebrates were in original metric scale, which cannot be used by ETC, and not in EQR scale, which is the requested determinand. 
	-	For half of the invertebrate stations the deteminand status class was also missing.  
	-	Minor QA issues on status class and spelling have been corrected by ETC

.. end-text1-placeholder
