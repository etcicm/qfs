
Country specific text about table S03, tables 26 and 27
-------------------------------------------


.. start-text1-placeholder

#. Data quantity issues:

	-	Estonia has reported rivers biology data from all three years 2010, 2011 and 2012 for the requested biological determinand PhytobenthosEQR. 
	-	Data have been reported from 2010 and 2011 for the requested biological determinand InvertebratesEQR. 
	-	Total number of river stations in the last year of reporting was 126 for phytobenthos and 310 for invertebrates. 
	-	The number of stations was decreased from 201 stations in 2010 to 126 stations in 2012 for phytobenthos, and stable for both the two years of reporting for invertebrates. 
	-	Data are reported from all RBDs (EE1, EE2, EE3) for both invertebrates and phytobenthos. Few stations reported were reported from EE3. 

#. Data quality issues: 

	-	For most stations, the data reported were seasonal (spring, summer, autumn) in 2010 and as growing season in the other year(s), rather than annual (which is recommended). 
	-	The reported EQR values for phytobenthos from the first two years could not be converted to normalised EQR values by the ETC due to problems with the classification system. 
	-	The reported mean values for invertebrates were in original metric scale, which cannot be used by ETC, and not in EQR scale, which is the requested determinand. 
	-	Minor QA issues on status class and spelling have been corrected by ETC

.. end-text1-placeholder
