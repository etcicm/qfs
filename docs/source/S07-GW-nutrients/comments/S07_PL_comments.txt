
About table 4-1 and 4-2
-----------------------

.. start-text1-placeholder

Poland reported aggregated data on nitrate, ammonium and nitrite from 1997 to 2003. About 30 - 40 groundwater 
quality monitoring stations were used for monitoring of each substance.

From 2004, disaggregated data were reported. 
Nitrate, ammonium and nitrite were reported in approximately identical number of stations as before in 2004 - 2007. 
For the period 2008 - 2011, data on nitrate, ammonium, nitrite, dissolved oxygen, orthophosphates, total organic carbon
and pH) were reported from 96 - 112 stations. Great step forward followed in 2012 - data from nearly 1100 stations 
were provided for all mentioned substances.

Data on total phosphates were not reported.

Up to 2007, all reported data represented 3 EIONET groundwater bodies. From 2008 to 2011, data from 28 - 37 WFD gw
bodies are used. In 2012, data for 160 WFD groundwater bodies are reported.

.. end-text1-placeholder



About table 4-3
---------------

.. start-text2-placeholder

Up to 2011, groundwater quality monitoring stations were located in several selected areas of Poland only.
In 2012, the entire area of Poland is covered by the reported stations (1163 stations).

The river basin districts listed in the table 4-3 for which no stations are available (PL3000 Swieza, PL4000 Jarft,
PL6700 Ucker) or RBDs with very low number of stations (PL1000 Danube 1 station and PL9000 Dniester 2 stations) 
are representing together very small part of the area of Poland, therefore this situation is acceptable.

.. end-text2-placeholder



About time series
-----------------

.. start-text3-placeholder

Poland reported disaggregated data on nitrate from 1995 to 2003 and from  2004 to 2012.
Data aggregated by country (1997 - 2003) are not reflected in the time series analysis.

57 stations have continuous time series for 5 years (2008 - 2012)
71 stations have continuous or nearly continuous (gap 1 year max) time series  for 4 years (either 2004 - 2007 or 2008 - 2012)
21 stations have continuous or nearly continuous (gap 1-2 years max) time series  for 3 years (either 2004 - 2007 or 2008 - 2012)
For 15 stations, data for 2 years are available
For 996 stations, data for 1 year are available (mostly 2012)
None of stations was monitored during both "subperiods" 2004 - 2007 and 2008 - 2012

No disaggregated data on nitrates were reported for 3 stations.

.. end-text3-placeholder
