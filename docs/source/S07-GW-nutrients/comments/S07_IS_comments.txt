
About table 4-1 and 4-2
-----------------------

.. start-text1-placeholder

Iceland reported disaggregated data on nitrate, nitrite and ammonium from 2004 (1 station in 2004 - 2006, 
2 - 3 stations from 2007 onwards).

Starting from 2007 - 2008, values on pH and concentrations of orthophosphates and total organic carbon 
are provided as well (1 - 2 stations).

.. end-text1-placeholder



About table 4-3
---------------

.. start-text2-placeholder

All groundwater monitoring stations are located in the capital Reykjavik or its close surrounding. Stations are linked to 
2 groundwater bodies (IS-Blafjoll and IS-Thingvellir).

The entire area of Iceland is represented by 1 river basin district IS1 Iceland

.. end-text2-placeholder



About time series
-----------------

.. start-text3-placeholder

Iceland reported disaggregated data on nitrates from 2004 for 5 stations together

1 station - continuous time serie 2008 - 2012
1 station - data reported for 2006 - 2007 and 2010 - 2012
1 station - data reported for 2004 - 2005 and 2008 - 2009
1 station - data reported for 2008 - 2009
1 station - data reported for 2007 only

.. end-text3-placeholder
