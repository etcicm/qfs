
About table 4-1 and 4-2
-----------------------

.. start-text1-placeholder

Hungary reported aggregated data on nitrate, nitrite and ammonium from 1989 to 2007. During this period, these substances 
were monitored by 800 - 1600 stations on the area of Hungary (cca 530 stations in 2006 only). 17 - 18 groundwater bodies 
were covered by these data.

From 2008 onwards, any data are missing and the SoE reporting was interrupted by country.

No data on other determinands in this category (orthophosphates, total phosphates, total organic carbon, pH)
were reported.

.. end-text1-placeholder



About table 4-3
---------------

.. start-text2-placeholder

No stations and no disaggregated data were reported, therefore the table 4-3 is empty for Hungary and can not be used for 
any analysis.

The entire are a of Hungaria is covered by 1 river basin district HU1000 Danube.

.. end-text2-placeholder



About time series
-----------------

.. start-text3-placeholder

No disaggregated data on nitrates were reported by Hungary.
Data aggregated by country  (1992 - 2007) can not be reflected in the time series analysis.

.. end-text3-placeholder
