
About table 4-1 and 4-2
-----------------------

.. start-text1-placeholder

Sweden reported aggregated data on nitrate, ammonium and dissolved oxygen from 1984 to 2006 with gap for 
all 3 substances in 1999 and for dissolved oxygen in 2001. 
Aggregated data on nitrite were reported from 1984 to 1995.
Reported aggregated data were provided for max. 8 stations and max. 3 groundwater bodies per substance and year.

From 2007 onwards, disaggregated data were reported. Data on nitrate, ammonium and dissolved oxygen are available from
2007 to 2012, data on pH, total organic carbon and total phosphates from 2008 to 2012.
During the period 2007 - 2009, the reported disaggregated data represented 22 - 24 stations for each determinand,
in 2010 55 - 65 stations, in 2011 34 - 44 stations and in 2012 60 - 100 stations. Therefore, the number of stations 
and groundwater bodies for which groundwater quality monitoring was reported is very variable.

.. end-text1-placeholder



About table 4-3
---------------

.. start-text2-placeholder

Groundwater monitoring stations (more than 200) are located on the entire area of Sweden with higher density 
in central and Southern part of the country.

The number of groundwater monitoring stations avaiable per river basin district is corresponding with the area and location
of the RBDs. The most of stations is located in the largest RBDs (SE1, SE2, SE3 ,SE4 and SE5). RBD SE1TO is located 
in northernmost part of Sweden, therefore 1 station in this RBD could be sufficient. 
The area of remaining RBDs (SENO1102, SENO1103, SENO1104 and SENO5101 is very small and all these RBDs are mostly located 
in the mountain area adjacent to the Norwegian boundary.

.. end-text2-placeholder



About time series
-----------------

.. start-text3-placeholder

Sweden reported disaggregated data on nitrates during the period 2007 - 2012.
Data aggregated by country (1992 - 1998, 2000 - 2006) are not reflected in the time series analysis.

21 stations have continuous time series for 6 years (2007 - 2012), 1 station for 5 years (2008 - 2012) and 18 stations 
for 3 years (2010 - 2012).
For 3 stations data from 2 years are available
Data sampled in 1 year only (during the period 2010 - 2012) were reported for 85 stations.

For remaining 79 stations, no disaggregated data on nitrates were reported.

.. end-text3-placeholder
