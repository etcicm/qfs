
About table 4-1 and 4-2
-----------------------

.. start-text1-placeholder

Kosovo reported disaggregated data on nitrate, nitrite, ammonium and on pH in 2006, 2008 and 2009.
Dissolved oxygen was reported in 2006 only.

Data from 16 stations were provided in 2006, from 2 stations in 2008 and from 21 stations in 2009.

Dataset reported in 2006 represented 3 groundwater bodies, in 2008 and 2009 1 groundwater body only.

.. end-text1-placeholder



About table 4-3
---------------

.. start-text2-placeholder

Groundwater monitoring stations are grouped in 3 - 4 areas located in north-western and south - eastern part
of the country. Therefore, many areas of the country are still not represented in the 
reported datasets.

Official delineation of river basin districts was not provided up to now by country.

.. end-text2-placeholder



About time series
-----------------

.. start-text3-placeholder

Kosovo reported disaggregated data on nitrates in 2006 and in 2008 - 2009.

For each station data monitored in 1 year only are available.

.. end-text3-placeholder
