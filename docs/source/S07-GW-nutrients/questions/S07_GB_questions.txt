
**Clarifying questions on data in current database**

#. Can you redeliver the disaggregated data to replace the aggregated data reported in the past for the period 1996 - 2004?

#. Are the disaggregated data monitored before 2000 available?


**Improving coverage of determinands, temporal and spatial coverage**

#. Can you provide data on ammonium, dissolved oxygen and orthophosphates for an analogous set of stations as for monitoring of nitrates and nitrites, if such data are available?

#. Can you provide the data on selected determinands for specified periods when no or very few data are available (ammonium 1999 - 2000 and 2005, dissolved oxygen 1996 - 2001 and 2005, orthophosphates before 2000, total phosphorus for any period)?

#. Can you provide data monitored in Northern Ireland before 2009 and in Scotland before 2006?



