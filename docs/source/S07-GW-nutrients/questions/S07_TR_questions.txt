
**Clarifying questions on data in current database**

#. Can you provide disaggregated data on nitrate, nitrite, ammonium and dissolved oxygen for 2005 - 2007 and 2009 to replace aggregated data reported in the past?

#. Can you add the disaggregated data on ammonium for 2011 - 2012 and on dissolved oxygen for 2010 - 2012 to fill existing gaps in the reporting of these data?


**Improving coverage of determinands, temporal and spatial coverage**

#. Can you add data on orthophosphates and total phosphates if these determinands are monitored?

#. Can you provide the data for the additional groundwater monitoring stations representing currently not covered areas of Turkey, if such stations exist?

#. Is the official delineation of river basin districts planned in the near future?

