
**Clarifying questions on data in current database**



**Improving coverage of determinands, temporal and spatial coverage**

#. Can you provide data on preferred SoE nutrients monitored before 2003, if these data are available?

#. Can you provide data on dissolved oxygen and total phosphates, if these determinands are monitored?

#. Are there additional groundwater quality monitoring stations available, especially in SW and SE part of Serbia? If yes, can you provide data from these stations, to get representative results for the entire area of Serbia?

