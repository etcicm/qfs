
**Clarifying questions on data in current database**

#. Are the disaggregated data of nitrates, nitrites, ammonium and dissolved oxygen sampled before 2006 available?


**Improving coverage of determinands, temporal and spatial coverage**

#. Could you provide the concentrations of total phosphorus, which are missing up to now and values of Orthophosphates before 2008, if available? 

#. The area of river basin district BG4000 (West Aegean RBD) is about 1/4 - 1/3 of RBDs BG1000 or BG3000, but the number of stations in which the data for preferred SoE nutrients (nitrate, nitrite, ammonium, dissolved oxygen) are monitored is cca 6 x lower. Are the 6 gw monitoring stations currently used in the RBD BG4000 representative for the entire area of this RBD? Can data for more stations in this RBD be delivered?

