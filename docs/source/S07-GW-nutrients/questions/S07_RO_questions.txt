
**Clarifying questions on data in current database**

#. Can you deliver the disaggregated data on preferred SoE nutrients (nitrate, nitrite, ammonium, dissolved oxygen) before 2012, to replace aggregated data reported in the past?


**Improving coverage of determinands, temporal and spatial coverage**

#. Can you provide data on orthophosphates and total phosphates, if these determinands are monitored?

#. Are there additional groundwater monitoring stations available in the areas of Romania with the low number of stations? If yes, can you add the data from these stations?

