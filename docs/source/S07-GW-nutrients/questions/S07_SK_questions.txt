
**Clarifying questions on data in current database**

#. Can you provide disaggergated data before 2001, to replace aggregated data reported in the past?

#. Can you add data on dissolved oxygen in 2001, if available?


**Improving coverage of determinands, temporal and spatial coverage**

#. Can you provide data on orthophosphates and total phosphates, if these determinands are monitored?
