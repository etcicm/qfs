
**Clarifying questions on data in current database**

#. AggregationPeriod is usually Annual, but in 2009-2010 it is reported as Autumn for certain determinands and stations, without parallel reporting of Annual data. Is this due to differences in methodology, or only in the reporting? Can the autumn data be regarded as representing annual data, or if not, do annual data exist? 

#. There is generally some ambiguity regarding the determinands Ammonium vs Total ammonium in the database. You have delivered Total ammonium data. Do you define this as NH4+ or the sum of NH4+ and NH3? 

#. Sampling depth is not reported for 2003-2006. Can the data in these years be assumed to be for surface samples? In 2009-2012 a range of sampling depths are reported. Is this reporting correct? 

**Improving coverage of determinands, temporal and spatial coverage**

#. There are no data before 2003, there are gaps in some of the time series and there is one very short series. Moreover, some determinands are missing before 2008 (e.g. total nitrogen and chlorophyll a) or after 2006 (e.g. Secchi depth), and some determinands are missing for some stations in certain years (e.g. total phosphorus). So - can data be redelivered for stations and determinands where data are missing? And are older data available, back to 1992? This is of particular interest for BOD5, total ammonium, nitrate, total nitrogen, orthophosphates, total phosphorus, Secchi depth and chlorophyll a. In practice, station time series that start late, end early or have long gaps cannot be included in the trend analysis for a given determinand.
   
#. Are the stations representative with respect to geographical and pressure gradients? If not, in which way can they be believed to be biased, and can data be delivered for more stations to reduce the bias?

#. All the WaterBodyIDs delivered do not seem to be identical to the EUSurfaceWaterBodyCode assigned to water bodies in the WFD reporting. Is this right? Can correct WaterBodyIDs be redelivered?

**Links/references**

Please provide further references to

#. National water quality reports.

#. National water quality indicators.

#. National water quality data sets or databases.