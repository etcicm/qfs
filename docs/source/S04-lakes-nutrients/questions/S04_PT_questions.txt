
**Clarifying questions on data in current database**

#. Are you aware of cases where the same station has been assigned different WaterbaseIDs, e.g. due to changes in station codes or station names in past deliveries? In case, please supply a list of such cases, so that data from the same station can be combined into one time series.

#. For 2007-2009 there are data on total organic carbon, while in 2011 dissolved organic carbon is reported. Is this due to an actual change in methodology, or can it be a reporting error?

#. There is generally some ambiguity regarding the determinands Ammonium vs Total ammonium in the database. You have delivered Total ammonium data. Do you define this as NH4+ or the sum of NH4+ and NH3? 

#. Sampling depth is not reported, except in 2009, where it is reported as zero. Can all data be assumed to be for surface samples?

**Improving coverage of determinands, temporal and spatial coverage**

#. There are no data before 2006, and there are frequently years missing from the time series. Moreover, some determinands are missing in some years, especially 2011-2012, but also e.g. total nitrogen in 2006 and Secchi depth after 2008, and some determinands are missing for some stations in certain years (e.g. total phosphorus is usually available for fewer stations). So - can data be redelivered for stations and determinands where data are missing? And are older data available, back to 1992? This is of particular interest for BOD5, total ammonium, nitrate, total nitrogen, orthophosphates, total phosphorus and chlorophyll a. In practice, station time series that start late, end early or have long gaps cannot be included in the trend analysis for a given determinand.
   
#. Are the stations representative with respect to geographical and pressure gradients? If not, in which way can they be believed to be biased, and can data be delivered for more stations to reduce the bias? E.g. there are no data from RBD PTRH1.

#. Are the WaterBodyIDs delivered identical to the EUSurfaceWaterBodyCode assigned to water bodies in the WFD reporting? If not, can correct WaterBodyIDs be redelivered?

**Links/references**

Please provide further references to

#. National water quality reports.

#. National water quality indicators.

#. National water quality data sets or databases.