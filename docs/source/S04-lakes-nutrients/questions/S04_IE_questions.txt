
**Clarifying questions on data in current database**

#. From 1992-2004 much of the data are reported with AggregationPeriod Summer only (or sometimes Winter in 2002). Is this due to a difference in methodology or only in the reporting? Can the seasonal data be regarded as representing annual data, or if not, do annual data exist?

#. Some time series end in 2006 and many start in 2007. Are you aware of cases where the same station has been assigned different WaterbaseIDs, e.g. due to changes in station codes or station names in past deliveries? In case, please supply a list of such cases, so that data from the same station can be combined into one time series. 

#. In 2010 and 2011 organic carbon is delivered either as dissolved or total. Is this due to a difference in methodology or can it be an error in the reporting? In 2012 only dissolved organic carbon data are delivered.

#. There is generally some ambiguity regarding the determinands Ammonium vs Total ammonium in the database. You have mainly delivered Total ammonium data, but for 2007-2008 and 2010-2012 Ammonium data are delivered. Do these data actually represent different methodology? In case, please explain. If not - have you reported NH4+ or the sum of NH4+ and NH3? 

#. There is no information on sampling depth for 1992-2005. Since then, sampling depth is reported as zero if available. Can it be assumed that all data represent surface samples?

**Improving coverage of determinands, temporal and spatial coverage**

#. Some time series end early (e.g. 2006) or start late (e.g. 2007), and there are gaps in some time series. Moreover, some determinants are reported for fewer stations and years (e.g. BOD5, total nitrogen) or is missing in some years (e.g. cholorophyll a in 2012). So - can data be redelivered for stations and determinands where data are missing over the period 1992-present? This is of particular interest for BOD5, total ammonium, nitrate, total nitrogen, orthophosphates, total phosphorus, chlorophyll a and Secchi depth. In practice, station time series that start late, end early or have long gaps cannot be included in the trend analysis for a given determinand.
   
#. Are the stations representative with respect to geographical and pressure gradients? If not, in which way can they be believed to be biased, and can data be delivered for more stations to reduce the bias? E.g. there is far less data from RBDs GBNIIENB and UKGBNIIENW.

#. Are all WaterBodyIDs delivered identical to the EUSurfaceWaterBodyCode assigned to water bodies in the WFD reporting? If not, can correct WaterBodyIDs be redelivered?

**Links/references**

Please provide further references to

#. National water quality reports.

#. National water quality indicators.

#. National water quality data sets or databases.