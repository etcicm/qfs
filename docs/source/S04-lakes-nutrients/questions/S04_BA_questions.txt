
**Clarifying questions on data in current database**

#. For 2007-2009 data have been reported with AggregationPeriod GrowingSeason or Summer instead of Annual. Is this due to a change in methodology or only in the reporting? Can the seasonal data be regarded as representing annual data, so that these can be used to fill the gaps in the annual time series? Or if not, do annual data exist for these years/stations?

#. Are you aware of cases where the same station has been assigned different WaterbaseIDs, e.g. due to changes in station codes or station names in past deliveries? In case, please supply a list of such cases, so that data from the same station can be combined into one time series.

#. Was there a change in COD methodology in 2008, from mainly measuring CODMn to mainly measuring CODCr? I.e. is the reporting of the two different determinands correct?

#. There is generally some ambiguity regarding the determinands Ammonium vs Total ammonium in the database. You have reported Total ammonium. By this, do you mean NH4+ or the sum of NH4+ and NH3? 

#. Sampling depth was only reported in 2008-10 and 2012, and only in 2008 was it reported as different from 0 (1 and 3 m). Is there in fact different methodologies, or can all data be regarded as surface samples?

**Improving coverage of determinands, temporal and spatial coverage**

#. Reporting starts in 2003 or later, and many time series end in 2008 and have generally few data. There are also years where certain determinands are missing from the reporting or reported for fewer stations (e.g. total nitrogen, total phosphorus, chlorophyll a, Secchi depth). So - can data be redelivered for stations and determinands where data are missing over the period 1992-present? This is of particular interest for BOD5, total ammonium, nitrate, total nitrogen, orthophosphates, total phosphorus, Secchi depth and chlorophyll a. In practice, station time series that start late, end early or have long gaps cannot be included in the trend analysis for a given determinand.
   
#. Are the stations representative with respect to geographical and pressure gradients? If not, in which way can they be believed to be biased, and can data be delivered for more stations to reduce the bias?

**Links/references**

Please provide further references to

#. National water quality reports.

#. National water quality indicators.

#. National water quality data sets or databases.
