
**Clarifying questions on data in current database**

#. Are you aware of cases where the same station has been assigned different WaterbaseIDs, e.g. due to changes in station codes or station names in past deliveries? In case, please supply a list of such cases, so that data from the same station can be combined into one time series.

#. There is generally some ambiguity regarding the determinands Ammonium vs Total ammonium in the database. You have mainly reported Total ammonium, but in 2006-2009 data were reported as Ammonium instead of or in addition to Total ammonium. Do these data actually represent different methodology? In case, explain. If not - have you reported NH4+ or the sum of NH4+ and NH3? 

**Improving coverage of determinands, temporal and spatial coverage**

#. There are no data from 1992 and 2011-2012, and there are also some gaps in the time series and time series ending early (e.g. in 2004-2008). Moreover, few data are available for some determinands (e.g. BOD5) and for some determinands data are available from fewer stations than other determinands (e.g. chlorophyll a). So - can data be redelivered for stations and determinands where data are missing over the period 1992-present? This is of particular interest for BOD5, ammonium, nitrate, total nitrogen, orthophosphates, total phosphorus and chlorophyll a. In practice, station time series that start late, end early or have long gaps cannot be included in the trend analysis for a given determinand.
   
#. Are the stations representative with respect to geographical and pressure gradients? If not, in which way can they be believed to be biased, and can data be delivered for more stations to reduce the bias?

**Links/references**

Please provide further references to

#. National water quality reports.

#. National water quality indicators.

#. National water quality data sets or databases.
