Lakes - Nutrients, Organic Matter and General Physico-Chemical Determinands
============================================================================

About table 3-1
---------------

.. start-text1-placeholder

Latvia reported Annual data  from 1992 to 2009. As addition, data were reported also for other aggregation periods.

Latvia has reported data for total of ten priority nutrients from 1992 to 2012. Two of these determinands (Nitrate and Total phosphorus) are covering the whole reporting period.

There is data for all the highest priority nutrients in the dataset.

.. end-text1-placeholder


About table 3-2
---------------

.. start-text2-placeholder

Lake water quality data has been reported by Latvia from 1992 to 2012 for 97 distinct lake stations. From 1992 to 1997 there were eight lake stations reported. The number increased up to 27 in 2005 and decreased down to 10 in 2008. Nevertheless, the number of reported lake stations increased up to 38 stations in 2012.

There are six RBDs in Latvia. All RBDs were covered with reported lake stations just in 2009.  For most of the years, lake data reported for 4-5 RBDs. There are six lake stations with the same coordinates which might indicate an error in coordinates.

Reported lake area is suspicious or illogical for three stations. Station coordinates fall outside the respective River Basin District on 14 lake stations.  Three stations are missing national station name  while 17 stations are missing lake name.

No sample depths have been reported until 2006. Sample  depth of 0.5 m have been reported for years 2006, 2007, 2008, 2009 and 2011

.. end-text2-placeholder


About time series
-----------------

.. start-text3-placeholder

Two lake station have broken time series (with gaps) covering reporting period from 1992 to 2011. One lake station have continuous (without gaps) time series covering reporting  period from 1992 to 2005. Eight stations have broken (with gaps) time series covering reporting periods longer than 10 years.

.. end-text3-placeholder