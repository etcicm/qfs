Lakes - Nutrients, Organic Matter and General Physico-Chemical Determinands
============================================================================

About table 3-1
---------------

.. start-text1-placeholder

The Former Yugoslav Republic of Macedonia  reported Annual data  from to 1997 to 2012 (no annual data were reported for 2004). Data for Summer period have been reported for 1992, 1995, 1998 and 2001.

The Former Yugoslav Republic of Macedonia  has reported data for total of six priority nutrients from 1992 to 2012 with Total phosphorus covering the whole reporting period.

In the dataset there are data for all priority nutrients except Orthophosphates.

.. end-text1-placeholder


About table 3-2
---------------

.. start-text2-placeholder

The Former Yugoslav Republic of Macedonia  has reported lake water quality data from 1992 to 2012 for 15 distinct lake stations. Until 2000, just one lake station have been reported. From 2000 till 2010 the number of reported lake stations was never greater than two. Nevertheless, six lakes stations have been reported in 2010 and eight lake stations have been reported in 2015.

Since The Former Yugoslav Republic of Macedonia is not an EU member there are no official RBDs designated for that region.

Six lake stations are missing national station name. Otherwise there are no major lake station issues.

Four distinct sample depths have been reported by The Former Yugoslav Republic of Macedonia  since 2006. Before that, no sample depths have been reported from The Former Yugoslav Republic of Macedonia .

.. end-text2-placeholder


About time series
-----------------

.. start-text3-placeholder

One lake station have continuous (without gaps) time series covering  reporting  period from 1992 to 2011. One station have broken (with gaps) time series covering the reporting period  from 2000 to 2009. There are 13 stations which have been reported just one year.

.. end-text3-placeholder