Lakes - Nutrients, Organic Matter and General Physico-Chemical Determinands
============================================================================

About table 3-1
---------------

.. start-text1-placeholder

Annual lake data have been reported by Slovenia from 1992 to 2010.  Data have been reported also for GrowingSeason and Summer aggregation periods.

Slovenia has reported data for total of nine priority nutrients from 1992 to 2012 with Nitrate, Orthophosphates and Total phosphorus covering the whole period.


.. end-text1-placeholder


About table 3-2
---------------

.. start-text2-placeholder

Slovenia has reported lake water quality data from 1992 to 2012 for 19 distinct lake stations.  For the first 11 reporting years  Slovenia reported four lake stations. The number then increased up to 12 in 2003 and dropped down to two in 2006. For the last three reporting years the number of reported lake stations was equal to 11.

There are two RBDs in Slovenia. Slovenia is reporting lake data for both of them. 

There are no major issues with the lake stations in Slovenia.

No sample depths have been reported by Slovenia until 2006. Since then, 33 distinct sample depths have been reported from Slovenia (0.2  m-2 m).

.. end-text2-placeholder


About time series
-----------------

.. start-text3-placeholder

There is just one lake station with continuous time series (no gaps) covering the whole reporting period from 1992 to 2012. 

.. end-text3-placeholder