Lakes - Nutrients, Organic Matter and General Physico-Chemical Determinands
============================================================================

About table 3-1
---------------

.. start-text1-placeholder

Ireland reported Annual data  from 1992 to 2012. Data for Summer period have also been reported from 1992 to 2004. In addition, data for Winter period have been reported for 2002.

Ireland has reported data for total of eight priority nutrients from 1992 to 2012. One of these determinands (Total phosphorus) is covering the whole reporting period.


.. end-text1-placeholder


About table 3-2
---------------

.. start-text2-placeholder

Lake water quality data has been reported by Ireland from 1992 to 2012 for 99 distinct lake stations. In 1992, there were 12 lakes stations reported. The number of reported lake stations was steadily increasing until 2000 when it reached  32 stations and remained stable for the next three years. A constant decrease down to 20 lake stations in 2005 followed. Since 2007, number of reported lake stations is more or less stable reaching 73 stations.

There is only seven RBDs in Ireland. In 2007 and 2008 lake data covers all seven RBD while in most other years, lake data covers five RBDs.

There are no major issues with the Irish lake stations.

No sample depths have been reported by Ireland until 2006. Since then, Ireland is reporting sample depth (0 m) for all lake stations.

.. end-text2-placeholder


About time series
-----------------

.. start-text3-placeholder

Two lake stations have continuous time series (without gaps) covering the whole reporting period from 1992 to 2012. 48 lake stations have continuous time series  covering reporting period from 2007 to 2012. Two lake stations have broken time series (with gaps) covering the whole reporting period from 1992 to 2012.

.. end-text3-placeholder