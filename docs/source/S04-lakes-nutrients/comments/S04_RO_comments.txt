Lakes - Nutrients, Organic Matter and General Physico-Chemical Determinands
============================================================================

About table 3-1
---------------

.. start-text1-placeholder

Annual data has been reported by Romania from 2006 to 2012.

Romania started reporting WISE SoE lake data in 2003.  It has reported Annual data from 2003 to 2012 and Autumn period data for 2009 and 2010.
Romania has reported data for total of 7 priority nutrients from 2003 to 2012 with BOD5, Nitrate, Total ammonium and Total phosphorus covering the whole period.


.. end-text1-placeholder


About table 3-2
---------------

.. start-text2-placeholder

Romania has reported lake water quality data from 2003 to 2012 for 17 distinct lake stations.  The number of reported lake stations stayed more or less constant over the reported years. For each year (except 2009 and 2010 when 15 stations have been reported) ,16 lake stations have been reported from Romania.

There is just one RBD in Romania. All reported stations are corresponding with it.

There are five stations which have same coordinates (it might indicate an error in coordinates). In addition, lake Area is suspicious or illogical on one lake station.

No sample depths have been reported by Romania until 2009. Since then, 40 distinct sample depths have been reported (from 0.1 to 17.8).

.. end-text2-placeholder


About time series
-----------------

.. start-text3-placeholder

There are 14 lake stations with continuous time series (no gaps) covering the whole reporting period from 2003 to 2012. 

.. end-text3-placeholder