Lakes - Nutrients, Organic Matter and General Physico-Chemical Determinands
============================================================================

About table 3-1
---------------

.. start-text1-placeholder

Annual data has been reported by Norway from 1992 to 2012.

Norway has reported data for total of five priority nutrients from 1992 to 2012 with Nitrate covering the whole reporting period.

In the dataset there are no data for BOD5 / BOD7, CODCr / CODMn and Orthophosphates.

.. end-text1-placeholder


About table 3-2
---------------

.. start-text2-placeholder

Norway has reported lake water quality data from 1992 to 2012 for 210 distinct lake stations.  For the first three reporting years 86 lake stations have been reported. Between 1995 and 2004 the number of reported lake stations varied around 100. Then it first increased up to 146 in 2005 and decreased down to 53 in 2012.

There are 17 RBDs in Norway. Since 1992 lake data has been reported for 11 RBDs (for most years 10 RBDs). 2005 is the only reporting year when lake data has been reported all 11 RBDs.

20 lake stations  fall outside the respective River Basin District. 41 lake stations are missing lake name.

Since 1992, only one depth (0 m) has been reported for the Norwegian lake stations.

.. end-text2-placeholder


About time series
-----------------

.. start-text3-placeholder

There three lake stations with continuous time series (no gaps) covering the whole reporting period from 1992 to 2012. There are also 59 lake stations with continuous time series (no gaps) covering  period from 1992 to 2011. 11 lake stations have broken (with gaps) time series
covering the whole reporting period from 1992 to 2012.

.. end-text3-placeholder