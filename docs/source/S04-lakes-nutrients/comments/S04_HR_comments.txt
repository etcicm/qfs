Lakes - Nutrients, Organic Matter and General Physico-Chemical Determinands
============================================================================

About table 3-1
---------------

.. start-text1-placeholder

Croatia reported Annual data for the reporting period from 2003 to 2012. However, no Annual data have been reported in 2005 and 2011.
GrowingSeason period data have been reported from 2010 to 2012. Summer period data have been reported for 2005 and 2006. In addition, Winter period data have been also reported in 2006.

Croatia has reported data for total of seven priority nutrients from 1992 to 2012. Six of these determinands (BOD5, CODMn, Nitrate, Orthophosphates, Total ammonium, Total phosphorus) are covering the whole reporting period.


.. end-text1-placeholder


About table 3-2
---------------

.. start-text2-placeholder

Lake water quality data has been reported by Croatia from 2003 to 2012 for nine distinct lake stations. The number of nine reported lake stations remains quite stable over the years except for years 2004  (three stations were reported) and 2011 when eight lake stations were reported.

Since Croatia became a full EU member in 2013, no official RBDs have been designated yet.

Reported lake area is suspicious or illogical on one lake stations. Nevertheless, there are no major issues with the Croatian lake stations.

No sample depths have been reported by Croatia until 2008. Since then, Croatia is reporting sample depth (0 m) for all lake stations.

.. end-text2-placeholder


About time series
-----------------

.. start-text3-placeholder

Three lake stations have continuous time series (without gaps) covering the whole reporting period from 2003 to 2012. Remaining six lake stations have broken time series (with gaps) covering the whole reporting period from 2003 to 2012.

.. end-text3-placeholder