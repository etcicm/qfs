.. exceltable:: Table 6.4: Hazardous substances emissions from diffuse sources: number of spatial units with values reported by determinand and year
	:file: ../../S10-Emissions/tables/S10_ZZ_table64.xls
	:header: 1
	:sheet: 3 