﻿.. exceltable:: Table 2.7: ﻿Number of observations reported, by BQE, River Basin District and year
	:file: ../../S03-Rivers-Biology/tables/S03_ZZ_table27.xls
	:header: 1
	:sheet: 3