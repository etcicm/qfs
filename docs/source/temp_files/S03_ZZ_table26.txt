.. exceltable:: Table 2.6: Number of observations reported, by determinand, aggregation period and year
	:file: ../../S03-Rivers-Biology/tables/S03_ZZ_table26.xls
	:header: 1
	:sheet: 3	