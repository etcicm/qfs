 


   
**Improving coverage of determinands, temporal and spatial coverage**

#.   Do you have any data from point sources?

#.   Do you model or calculate emissions data from diffuse sources?

#.   If yes, what is the main obstacle to provide emission data to SoE?

#.   What could help you to provide emission data to SoE from EEA side?




**Links/references**

Please provide further references to

#.   National water emissions and source apportionment reports;
#.   National water emissions indicators:
#.   National water emissions data sets - databases;

