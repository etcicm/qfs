

   
**Improving coverage of determinands, temporal and spatial coverage**

#.   Do you collect and store data about industrial non E-PRTR discharges?

#.   Do you model or calculate emissions data from diffuse sources?

#.   If yes, what is the main obstacle to provide emission data to SoE?

#.   What could help you to provide emission data to SoE from EEA side?

#.   Would you plan to use SoE Emission reporting after better harmonisation with WFD reporting? Would a better alignment with the code list from the WFD pressure list facilitate future reporting? 




**Links/references**

Please provide further references to

#.   National water emissions and source apportionment reports;
#.   National water emissions indicators:
#.   National water emissions data sets - databases;

