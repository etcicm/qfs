**Clarifying questions on data in current database**


#. Could you please identify BOD emissions reported on 2006 (BOD5 or BOD7)?

#. Could you please provide geographical layer for the spatial unit used for SoE Emissions (river basin district as for E-PRTR - or other river basin delineation ? This should be along with a code list for the spatial unit used.

#. Could you please provide a code list for the of validated river basins?  

#. The ETC data report on emissions of pollutants..(chpt. 4.3) identify inconsistencies for Cd and Hg between E-PRTR and SoE data. Can this be explained ?

#.  Table 6.3 heading is missing lates year: 2013 ?

#.  Table 6.3 determinand:  HALOGENATED ORGANIC COMPOUNDS , does this correspond :Adsorbable organic halogens (AOX)  ?  

   
**Improving coverage of determinands, temporal and spatial coverage**

#.   Is it possible to provide time series for the emission data referring to same spatial units ?

#.   Is it possible to report according to the SoE Emissions data dictionary consistently ?  

#.   Would a better alignment with the code list from the WFD pressure list facilitate future reporting ? 




**Links/references**

Please provide further references to

#.   National water emissions and source apportionment reports;
#.   National water emissions indicators:
#.   National water emissions data sets - databases;

