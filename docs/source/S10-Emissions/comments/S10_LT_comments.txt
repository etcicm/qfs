About table 6-1 to 6-4
-----------------------

.. start-text1-placeholder
 
All emission  reported data cover the whole territory of country. All (nutrients, organics and hazardous substanes) point emission data were reported from five years: 2008 - 2012 and they include detailed apportionment of sources in each year. Hazardous substances emissions were provided for 21 pollutants (including 13 metals. 
BOD7 and phosphorus diffuse emissions were reported from 2008 and 2009, but because one RBD was reported for 2008 only and remaining RBDs 2009, they should be identified as 2008 - 2009 period data. Diffuse emissions were calculated as total diffuse, agricultural, un-connected dwellings and background emissions.
Lithuania reported no hazardous substances from diffuse sources.


.. end-text1-placeholder