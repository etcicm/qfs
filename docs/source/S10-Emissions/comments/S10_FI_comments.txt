About table 6-1 to 6-4
-----------------------

.. start-text1-placeholder
 
All emission  reported data cover the whole territory of country. Point emission data were reported from three years - 2007, 2008 and 2012. Emission source categories include industrial, municipal and other sources each reported year. Hazardous substances emissions were provided for 13 pollutants (including 8 metals). 
Nutrient (nitrogen and phosphorus) diffuse emissions were reported from one period - 2000 - 2006 and they were calculated as agricultural, atmospheric deposition, storm overflows, background and other diffuse emissions.
Finland reported no hazardous substances from diffuse sources.


.. end-text1-placeholder