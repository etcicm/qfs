About table 6-1 to 6-4
-----------------------

.. start-text1-placeholder
 

All emission reported data cover the whole territory of the country. Nutrient, organics and hazardous substances point emissions were reported from 2006 - 2012 with detailed apportionment of municipal treated discharges, direct discharges to coastal and transitional water, industrial discharges and emissions to groundwater. 
Point emissions of hazardous substances were provided for 49 pollutants includong 18 metals Slovenia reported no diffuse emissions.


.. end-text1-placeholder