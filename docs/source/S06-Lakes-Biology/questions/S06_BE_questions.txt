
**Clarifying questions on data in current database**

#. How often are you able to report the same BQE for the same station: annually, every two years, every three years or only once every six years? Is this frequency different for phytoplankton than for macrophytes?


**Improving coverage of determinands, temporal and spatial coverage**

#. Can data be reported from more recent years?
#. Can additional metrics for phytoplankton (chlorophyll and cyanobacteria) and macrophytes (growing depth or % cover) be reported? 
#. Is it possible to increase the number of stations reported?
#. Can data be reported from all the Belgian RBDs in the future in order to ensure geographic representativeness? 
#. If more stations are reported, is it possible to ensure consistency with the WFD status class distribution for the specific BQEs? (to avoid that the SoE stations are biased towards too many in high or good status or too many in moderate or worse status?)

**Links/references**

Please provide further references to

#. National water biology reports

#. National water biology indicators


