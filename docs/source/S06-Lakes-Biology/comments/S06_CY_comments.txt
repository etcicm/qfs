
About table S06, tables 36 and 37
-----------------------

.. start-text1-placeholder


Cyprus has reported lakes biology data from year 2010 to year 2012 (all years for this data request) for phytoplankton, including additional data for all requested parameters. 
In the dataset there are no data for determinand MacrophyteEQR nor for additional macrophytes data requested. 
Cyprus reported biology data in total from 8 lakes stations in the only RBD in this period. 
The number of annually reported lakes stations decreased from 8 stations in 2010 to 3 in 2011, but increased to 8 again in 2012.

There are minor quality issues that should be corrected:

The aggregation period reported is "Summer", but should preferably be reported as "Annual" (cf. Data Dictionary).

EQR data for phytoplankton:

-	All data reported have status class. 
-	Year 2011: EQR values are missing for a few stations. 
-	Year 2012: EQR values could not be normalised for most stations. The reason is that the stations are HMWB where H/G class boundaries are not defined. 
-	Classification system: H/G class boundary was defined as 0.8 for years 2010-2011, but was reported as "not available" for year 2012. 
-	Clarification is needed on the class boundaries for phytoplankton.

Additional data for phytoplankton:

-	Data for 2010-2011 had minor quality issues, which have been corrected. Data for 2012 had no quality issues!


.. end-text1-placeholder
