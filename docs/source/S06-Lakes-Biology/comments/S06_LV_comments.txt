
Country specific text about table S06, tables 36 and 37
---------------------------------------------

.. start-text1-placeholder

#. Data quantity

	-	Latvia has reported lakes biology data from all three years 2010-2012 for some of the requested additional determinands for phytoplankton (Chlorophyll_a and TotalPhytoplanktonBiomass).
	-	No data have been reported for the requested determinand PhytoplanktonEQR, nor for the requested additional determinand CyanobacteriaBiomass or CyanobacteriaProportion. 
	-	No data have been reported the the requested determinand MacrophytesEQR, nor for any of the additional determinands for macrophytes. 
	-	The total number of stations reported was 36 stations from 2012, which was an increase from previous years.	
	-	Data for were reported from all the four Latvian RBDs. 

#. Data quality

	-	Aggregation period was reported as summer, spring or growing season and not as annual.
	-	DeterminandStatusClass reported for original-scale metrics from 2011 will not be used by ETC.
	-	Minor QA issues on WaterbodyTypeIC (spelling) for data from 2010 have been corrected by ETC. 

.. end-text1-placeholder
