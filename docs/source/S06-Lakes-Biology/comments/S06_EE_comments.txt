
Country specific text about table S06, tables 36 and 37
---------------------------------------------

.. start-text1-placeholder

#. Data quantity

	-	Estonia has reported lakes biology data from the year 2010 for both the requested determinands PhytoplanktonEQR and MacrophytesEQR, but no data for any of the two determinands from 2011 and 2012.  
	-	Additional biological data for phytoplankton chlorophyll, total biomass and cyanobacteria from all three years 2010-2012.
	-	Additional biological data for macrophytes depth limit was reported from 2010 only. No data were reported for the other additional determinands for macrophytes.
	-	The total number of stations reported was 5 for MacrophytesEQR, and 8 for PhytoplanktonEQR.
	-	The total number of stations reported for Phytoplankton Chlorophyll a, TotalBiomass and cyanobacteria varied from 1 to 6 station in 2012 depending on aggregation period, while for Macrophytes depth limit 2 stations were reported in 2010.
	-	The lakes biology data are reported from only one of the three Estonian RBDs: EE2.

#. Data quality

	-	For MacrophytesEQR, the mean value was not EQR, but original scale, and cannot be used by ETC.
	-	DeterminandStatusClass was reported for original-scale metrics for both phytoplankton and macrophytes, but not for EQR metrics and will not be used by ETC.
	-	The aggregation period reported for Phytoplankton EQR and MacrophytesEQR is growing season, and not annual, as recommended by the ETC.
	-	The aggregation period reported for the additional data for phytoplankton as summer, growing season and annual. 
	-	Several QA issues on types and units have been corrected by ETC.


.. end-text1-placeholder
