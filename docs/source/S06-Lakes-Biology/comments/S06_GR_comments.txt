
Country specific text about table S06, tables 36 and 37
---------------------------------------------

.. start-text1-placeholder

#. Data quantity

	-	Greece has reported lakes biology data from the years 2011 and 2012 for the all the requested additional phytoplankton determinands (Chlorophyll_a, TotalPhytoplanktonBiomass, CyanobacteriaBiomass and CyanobacteriaProportion). 
	-	No data has been reported for the requested determinand PhytoplanktonEQR.
	-	No data has been reported for the requested determinand MacrophytesEQR, nor any additional data for Macrophytes.
	-	The total number of stations reported for Phytoplankton chlorophyll from 2012 was 33 stations, while for the other additional phytoplankton determinands, 19 stations was reported for aggregation period growing season.
	-	The total number of stations increased largely from 2011 to 2012.
	-	Data for were reported from all the Greek RBDs, except GR03, GR13 and GR14 in 2012. 

#. Data quality

	-	Aggregation period was reported as growing season, autumn or summer, but not as annual.
	-	Minor QA issues on spelling of type have been corrected by ETC.


.. end-text1-placeholder
