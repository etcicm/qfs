Rivers - Biology
================

**Use of data by the EEA**
--------------------------


The information will be used to assess the ecological status in a comparable way across Europe
and to identify potential problem areas at the European level. Assessments are also made periodically
on the impact of particular socio-economic sectors on water (e.g. the impact of agriculture on water),
and of particular issues (e.g. nutrients in European ecosystems). Such assessments will be
improved by the reporting of data on the biological elements of water bodies. The national EQR
values will be translated to normalised EQR values by EEA, based on the national EQR class
boundaries and a simple interpolation technique, to allow the data to become comparable
between countries and across regions.

The normalised data could be used to calculate a new EEA indicator, including for example:

*	Summaries of the normalised EQR values grouped into different status classes at
	different spatial scales: national, river basin district (RBD) or river basins (RBs).
*	Time series of the normalised EQR values for stations or water bodies aggregated
	for different European regions (e.g. GIG-regions) and different river types
	(e.g. small lowland rivers).
*	Proportion of river water bodies or stations within each country or within each
	RBD for which there are statistically significant increases, decreases and no changes in
	normalised EQR values over time.
*	Comparison of river nutrients with river biology for stations reported for both data flows.
	Correlations between Total P in rivers and phytobenthos normalised EQR, and between
	BOD and macroinvertebrate EQR (pressure-impact information)

**EEA products**

* `Waterbase - Rivers
  <http://www.eea.europa.eu/data-and-maps/data/waterbase-rivers-10>`__
* EEA reports
	*	`EEA ETC/ICM 2012: Ecological and chemical status and pressures. Thematic assessment for EEA Water 2012
		<http://icm.eionet.europa.eu/ETC_Reports/EcoChemStatusPressInEurWaters_201211>`__
* EEA maps
	*	`Biological elements in rivers and lakes
		<http://www.eea.europa.eu/themes/water/interactive/biological-water-quality-in-rivers>`__
	*	`Macroinvertebrates in rivers
		<http://www.eea.europa.eu/data-and-maps/explore-interactive-maps/macroinvertebrates-in-rivers>`__
	*	`Phytobenthos in rivers
		<http://www.eea.europa.eu/data-and-maps/explore-interactive-maps/phytobenthos-in-rivers>`__


**Reported data**
-----------------

*Note*: In the following section you will find some selected tables with information concerning the reporting in the period from 1992 to 2012 (if years are available). Some of the comments and questions refer to other tables (background tables). 
You will find these background tables as well as an overview on all background tables in your country folder.

Table 2.6 and Table 2.7 provide an overview of biology records per determinand, aggregation period and year resp.
per BQE, RBD and year for the country.


.. include:: /S03-Rivers-Biology/comments/ZZ.txt
   :start-after: start-text1-placeholder
   :end-before: end-text1-placeholder

.. include:: /S03-Rivers-Biology/tables/S03_ZZ_table26.txt
	
.. include:: /S03-Rivers-Biology/tables/S03_ZZ_table27.txt
   
*Note*: Important questions in reporting on biology are if status classes and EQR values are reported from member countries and what
the reasons are if they are not reported. Relevant is also if reported EQR values can be normalised or if there are problems in doing so.
In general care should be taken to ensure that the data are consistent with the WFD reporting. This means that the stations reported are
geographically representative, as well as representative in terms of the WFD distribution of ecological status classes, and that all major
river types are included.

*Questions regarding the reporting on biology in rivers*
--------------------------------------------------------

.. include:: /S03-Rivers-Biology/questions/ZZ.txt