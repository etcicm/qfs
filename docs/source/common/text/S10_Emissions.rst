
Emissions
********************

**Reporting obligation**

Data on emissions are collected through the WISE-SoE Reporting process (
`Water emission quality WISE 1
<http://rod.eionet.europa.eu/obligations/632>`_)
Following the test data request in 2008, data on emissions to water are requested as regular data flow from 2009 onwards.

The data requested on emissions include emissions of nutrients, organic matter and hazardous substances from a range of
both point and diffuse sources, as shown below.

.. image:: /common/images/emissions.jpg
   :width: 50%
   
There are different reporting processes for emission data:

*	Eurostat reporting on Inland waters (`Joint Questionnaire OECD Eurostat <http://rod.eionet.europa.eu/obligations/645>`_) - if countries reported data under emissions
	to water in e.g. 2013, this information will be used to pre-fill the 2014 Eurostat/OECD Joint Questionnaire and no Eurostat
	reporting has to be done by the country;

*	`Under the UWWT directive <http://rod.eionet.europa.eu/obligations/613>`_ and for `E-PRTR reporting <http://rod.eionet.europa.eu/obligations/538>`_;

*	Under the WFD (`WFD River Basin Management Plan including programme of measures <http://rod.eionet.europa.eu/obligations/521>`_).
	The `new WFD reporting guidance 2016 <http://icm.eionet.europa.eu/schemas/dir200060ec/resources2014>`_ for the 2nd reporting includes concrete references to SoE reporting.
	
*	Any data already submitted under E-PRTR point sources or SoE TCM waters (Riverine Load or Direct Discharges)
	reporting need not be resupplied under this reporting obligation.

	
	
	
	
	
Emissions
=========

**Use of data by the EEA**
--------------------------

Almost all human activities can and do impact adversely upon the water. Water quality is influenced by both direct point sources and
diffuse pollution, which comes from urban and rural populations, industrial emissions and farming. Member States are required to identify
significant point and diffuse sources of pollution in the River Basin District and the WFD requires that Member States collect and maintain
information on the type and magnitude of significant pressures. Member States are also obliged to prepare inventories for emissions,
discharges and losses for Priority and Hazardous Priority Substances.
The information reported will be used to formulate indicators of emissions that will be used in the assessment of pressures and states of Europe's waters.
These indicators will identify trends and help to evaluate the effectiveness of European policy and legislation.
During the preparation of new guidance for next WFD reporting, it has been incorporated that a source apportionment as reported with the
data dictionary of SoE emissions can be used for such purpose. In addition, improved assessment of the marine environment should result
from the quantification of emissions of land based pollutants (also in support of the work done by marine conventions).
The current EEA water resource efficiency indicators on emission intensities at country level on agriculture (WRE001),
domestic sector (WREI002) and manufacturing industry (WREI003) could potentially benefit from SoE emissions data, provided a better
data coverage than until now.  An analyses of emission data reported under European data flows has been reported by ETC/ICM where some comparisons between SoE emissions 
and other data flows have revealed inconsistencies. This may lead to corrections in SoE emission or these other data flows.

Newly developed indicators based on the requested data could include:

*	Time series of annual emissions and any derived indices aggregated at a RBD/RB scale for each individual source
	(e.g. urban, industry, agriculture).

*	Apportionment of emissions by source and by determinand within each national RBD/RB, showing where there are statistically
	significant increases, decreases and no changes over time. Such source apportionment identifies the key sectors to be targeted by measures


**EEA products**

*	`Waterbase - Emissions to water <http://www.eea.europa.eu/data-and-maps/data/waterbase-emissions-4>`_

*	Water resource efficiency indicators

	*	`Emission intensity of manufacturing industries in Europe (WREI 003) <http://www.eea.europa.eu/data-and-maps/indicators/emission-intensity-of-manufacturing-industries/assessment>`_
	*	`Emission intensity of domestic sector in Europe (WREI 002) <http://www.eea.europa.eu/data-and-maps/indicators/emission-intensity-of-domestic-sector/assessment>`_
	*	`Emission intensity of agriculture in Europe (WREI 001) <http://www.eea.europa.eu/data-and-maps/indicators/untitled-indemission-intensity-of-agriculture/assessment>`_
	
	
*	EEA reports

	*	`EEA2005: Source apportionment of nitrogen and phosphorus inputs into the aquatic environment <http://www.eea.europa.eu/publications/eea_report_2005_7>`_
	*	`EEA 2010: Freshwater quality - SOER 2010 thematic assessment <http://www.eea.europa.eu/soer/europe/freshwater-quality>`_
	*	`EEA 2011: Hazardous substances in Europe's fresh and marine waters - An overview <http://www.eea.europa.eu/publications/hazardous-substances-in-europes-fresh>`_
	*	`ETC/ICM Technical Report 1/2014: Emissions of pollutants to Europe's waters -Analysis of data reported under European data flows <http://icm.eionet.europa.eu/ETC_Reports>`_

*	`European Pollutant Release and Transfer Register - E-PRTR <http://www.eea.europa.eu/data-and-maps/explore-interactive-maps/promotions/european-pollutant-release-and-transfer-register-2014-e-prtr>`_

**Reported data**
-----------------

*Note*: In the following section you will find some selected tables with information concerning the reporting in the period from 1992 to 2012 (if years are available). Some of the comments and questions refer to other tables (background tables). 
You will find these background tables as well as an overview on all background tables in your country folder.

In the following tables 6.1 - 6.4 there is an overview on reporting nutrients and hazardous substances from point sources and
diffuse sources and the groups of emission sources (in the background tables) which have been used in SoE Emissions.

*Note*: E-PRTR aggregated emission data, calculated by ETC are not included in the following tables.

.. include:: /S10-Emissions/comments/ZZ.txt
   :start-after: start-text1-placeholder
   :end-before: end-text1-placeholder

.. include:: /S10-Emissions/tables/S10_ZZ_table61.txt

.. include:: /S10-Emissions/tables/S10_ZZ_table62.txt

.. include:: /S10-Emissions/tables/S10_ZZ_table63.txt
   
.. include:: /S10-Emissions/tables/S10_ZZ_table64.txt

*Note*: The tables should give an overview if a country reports data on emissions from point and diffuse sources which determinands and which type of source apportionment have been reported.
In all tables the value means the number of spatial units in which the determinand was reported for that year.

*Questions regarding the reporting on emissions*
------------------------------------------------

.. include:: /S10-Emissions/questions/ZZ.txt




