
Lakes
*****

**Reporting obligation**

Data on lakes are collected through the WISE-SoE data collection process (`Lake quality EWN-2; <http://rod.eionet.europa.eu/obligations/29>`__).
Biological quality elements in lakes are integrated into reporting of water quality starting from 2012 reporting period.

The data requested on lakes includes the physical characteristics of the lake monitoring stations,
proxy pressures on the upstream catchment areas, as well as chemical quality data on nutrients and organic matter,
and hazardous substances in lakes.

It also includes the biological data (primarily calculated as national Ecological Quality Ratios EQR),
as well as information on the national classification systems for each biological quality element and waterbody type.
eterminands which should be reported under this data flow are shown in the following figure.


.. image:: /common/images/determinands_lakes.jpg
   :width: 80%

Closely related to the SoE reporting on lakes water quality is the reporting under the WFD (`WFD River Basin Management Plan including
programme of measures <http://rod.eionet.europa.eu/obligations/521>`__ ). The `new WFD reporting guidance 2016 <http://icm.eionet.europa.eu/schemas/dir200060ec/resources2014>`__ for the 2nd reporting includes concrete references to SoE reporting.
The information reported by EEA countries can only be interpreted reasonably when streamlined with the WFD reporting.
Combining water quality results with information on ecological status and potential, as foreseen in the WFD reporting guidance,
are only possible if the WFD water body is linked with SoE stations which provide long time series on relevant determinants.

Lakes - Nutrients, Organic Matter and General Physico-Chemical Determinands
===========================================================================


**Use of data by EEA**
----------------------

The information will be used to formulate indicators to assess the state and trend of the determinand and monitoring progress with
European policy objectives.

**EEA products**

* `Waterbase - Lakes <http://www.eea.europa.eu/data-and-maps/data/waterbase-lakes-10>`_
* EEA indicators
	*  	`Nutrients in Freshwater (CSI 020)
	   	<http://www.eea.europa.eu/data-and-maps/indicators/nutrients-in-freshwater/nutrients-in-freshwater-assessment-published-6>`__
* EEA reports
	* 	`EEA 2010: Freshwater quality - SOER 2010 thematic assessment.
		<http://www.eea.europa.eu/soer/europe/freshwater-quality>`__
	* 	`EEA ETC/ICM 2010: Freshwater Eutrophication Assessment - Background Report for EEA European Environment State and Outlook Report 2010.
		<http://icm.eionet.europa.eu/docs/Freshwater_eutrophication_background_report_29_Nov_2010_final4.pdf>`__
	* 	`EEA, 2012 European waters - assessment of status and pressures, EEA Report No 8/2012
		<http://www.eea.europa.eu/publications/european-waters-assessment-2012>`__
	* 	`EEA ETC/ICM 2012: Ecological and chemical status and pressures. Thematic assessment for EEA Water 2012
		<http://icm.eionet.europa.eu/ETC_Reports/EcoChemStatusPressInEurWaters_201211>`__
* EEA maps
	* 	`Total phosphorus in lakes
		<http://www.eea.europa.eu/data-and-maps/explore-interactive-maps/wise-soe-total-phosphorus-in-lakes>`__


**Reported data**
-----------------

*Note*: In the following section you will find some selected tables with information concerning the reporting in the period from 1992 to 2012 (if years are available). Some of the comments and questions refer to other tables (background tables). 
You will find these background tables as well as an overview on all background tables in your country folder.

**Determinands**


Table 3.1 provides an overview of the determinands reported per year.

.. include:: /S04-lakes-nutrients/comments/ZZ.txt
   :start-after: start-text1-placeholder
   :end-before: end-text1-placeholder	
   
.. include:: /S04-lakes-nutrients/tables/S04_ZZ_table31.txt

*Note*: In the current data set the reporting of some high priority determinands  has stopped or there has been a change in the determinands
in the database. EEA wants to clarify if these changes are real changes or it has been errors/misinterpretations introduced in compiling the
databases. In addition, the aim is to ensure that the high priority determinands (e.g. nitrate or orthophosphate) have as complete coverage as
possible.

**Station coverage**


Table 3.2 shows the number of lake stations by River Basin Districts which reported on nutrients per year.

.. include:: /S04-lakes-nutrients/comments/ZZ.txt
   :start-after: start-text2-placeholder
   :end-before: end-text2-placeholder
   
.. include:: /S04-lakes-nutrients/tables/S04_ZZ_table32.txt

*Note*: One aspect of the quality fact sheets is to improve the spatial coverage and ensure that stations are reported for all RBDs.

**Time series**

The trend evaluation in EEA water quality indicators (CSI20) is based on stations with data from the period 1992-2012 or 2000/2003-2012.

.. include:: /S04-lakes-nutrients/comments/ZZ.txt
   :start-after: start-text3-placeholder
   :end-before: end-text3-placeholder

Note: EEA water quality indicators are used for trend assessments based on consistent time series with some gap filling.
For a single country consistent time series are established for the defined period (e.g. 1992-2012; or 2000-2012) with some gap filling
(e.g. up to 3 years)  and only stations with values for all years in the defined period are used. This ensures that any trend is because of
change in the observations and not in the stations included. Sometimes there is a stop in reporting stations in some years; or stations
cannot be clearly identified, e.g. because of missing or changing station names.  EEA wants to clarify these issues and improve
the consistency of time series for high priority nutrients.


*Questions regarding the reporting on general water quality in lakes*
---------------------------------------------------------------------
.. include:: /S04-lakes-nutrients/questions/ZZ.txt


