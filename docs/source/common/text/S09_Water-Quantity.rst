
Water Quantity
********************

**Reporting obligation**

Data on freshwater resources availability, abstraction and use at regional spatial scale are collected through the WISE-SoE annual data flow
(`State and quantity of water resources EWN-4; <http://rod.eionet.europa.eu/obligations/184>`_). These data are primarily used to formulate indicators
(associated with the EEA's Core Set Indicators) and assess the state and trends of the water resources and associated pressures,
and monitor the progress with European policy objectives.

The information needed in relation to water quantity is generally described as drivers, pressures, state, impact, response.
In general there is a need for coherent European indicators describing water availability/scarcity in connection with water use and water use
efficiency to assess the extent and intensity of the problem, and of the impact of particular socio-economic sectors on water abstraction
(e.g. water abstraction by agriculture).


The main blocks of data collected through WISE-SoE are the following:

*	**Point data:** Individual measurements of streamflow, groundwater level and reservoir inflow/outflow within the specific reporting unit
	(e.g. RBD).
*	**Water Availability:** Components of the Water Balance as spatially aggregated data on a specific reporting unit such as
	hydrometeorological parameters (e.g. area precipitation, potential/actual evapotranspiration etc.), storage (e.g. snowpack,
	changes in reservoir and groundwater storage), returned flow, reused water, desalinated water, water imports/exports.
*	**Water Abstraction:** Total volume of freshwater abstraction from both surface water and groundwater for public water supply systems
	and for self-supply. Groundwater available for annual abstraction and evaporation losses (during transport and use).
*	**Water Use:** Total volume of freshwater used and breakdown by sector (according to NACE classes) provided by
	public water supply systems and self-supply as well as recycled water.

A detailed description of the requested data is given by the `Water Quantity Data Manual <http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/wise-soe-reporting-2013/water-quantity-reporting-2013/water-quantity-data-manual_v.3.1>`_

There are different reporting processes for water quantity data:

*	Eurostat reporting on Inland waters (`Joint Questionnaire OECD Eurostat <http://rod.eionet.europa.eu/obligations/645>`_) - if countries reported data under emissions
	to water in e.g. 2013, this information will be used to pre-fill the 2014 Eurostat/OECD Joint Questionnaire and no Eurostat
	reporting has to be done by the country;
*	Under the WFD (`WFD River Basin Management Plan including programme of measures <http://rod.eionet.europa.eu/obligations/521>`_).
	The `new WFD reporting guidance 2016 <http://icm.eionet.europa.eu/schemas/dir200060ec/resources2014>`_ for the 2nd reporting includes concrete references to SoE reporting.
	
Water Quantity
==============

**Use of data by the EEA**
--------------------------

The information will be used to formulate indicators used to assess the state and trends of the water resources and associated pressures, and monitor the progress with European policy objectives.
Furthermore the information needed in relation to water quantity can be used to identify and quantify (through specialized indicators) the current 
state of and the pressure and impacts on the water environment - how these are changing over time and whether the measures taken at different levels are effective.

Water quantity data are also needed for the monitoring of water transfers, consumptions, abstractions and returns in properly defined spatial
and temporal scales across Europe. This is feasible through the ongoing development of water accounts, which is a system of water quantity
balance estimation between the natural environment and the various economic units (households, industry, etc.) that utilize water.
The development of this system is very important in order to confront the future challenges of increasing water use demand and shortage of
water availability due to climate change with assessments on European level.

Another important issue for which water quantity data are needed is the increasing appearance of drought and water scarcity periods
especially in southern European Countries. In order to examine this phenomenon on European level in a more consistent way the indicator WEI+ has been developed.
This indicator calculates the water consumption against the renewable water resources, quantifying the sustainability of water use in country
or RBD scale. To make reasonable assessments also on European level a minimum spatial and temporal resolution would be desirable on European level. 
Regarding the spatial scale, RBD is the agreed resolution, but SU is preferable and optional. 
As for the time resolution; monthly and seasonal is necessary, daily is desirable, yearly is optional. 
In the following sections we are summarising the way data have been reported so far, the further discussion on an higher resolution reporting will be discussed 
in the context of the further development of the water accounts also under the WFD- CIS group for water accounts. The following questions are restricted to the 
correction and gap filling of already reported data.


**EEA products**

*	`Waterbase - Water quantity <http://www.eea.europa.eu/data-and-maps/data/ds_resolveuid/9C1E1BA6-01F2-482B-A350-4972A51BF0B4>`_

*	Indicators

	*	`Use of freshwater resources (CSI 018) <http://www.eea.europa.eu/data-and-maps/indicators/use-of-freshwater-resources/use-of-freshwater-resources-assessment-2>`_

*	EEA reports

	*	`EEA 2013: Results and lessons from implementing the Water Assets Accounts in the EEA area <http://www.eea.europa.eu/publications/water-assets-accounts-report>`_
	*	`EEA 2012: Water resources in Europe in the context of vulnerability <http://www.eea.europa.eu/publications/water-resources-and-vulnerability>`_
	*	`EEA 2012: Towards efficient use of water resources in Europe <http://www.eea.europa.eu/publications/towards-efficient-use-of-water>`_
	*	`EEA ETC/ICM 2012: Vulnerability to Water Scarcity and Drought in Europe - Thematic assessment for EEA Water 2012 Report <http://icm.eionet.europa.eu/ETC_Reports/VulnerabilityToWaterScarcityAndDroughtInEurope>`_
	*	`EEA 2010; Water resources: quantity and flows - SOER 2010 thematic assessment <http://www.eea.europa.eu/soer/europe/water-resources-quantity-and-flows>`_
	*	`EEA 2009: Water resources across Europe - confronting water scarcity and drought <http://www.eea.europa.eu/publications/water-resources-across-europe>`_

*	EEA maps

	*	`Water quantity - Location of Streamflow Gauging Stations <http://www.eea.europa.eu/themes/water/interactive/water-quantity-location-of-streamflow>`_

In addition, results on water quantity are included into EEA products on climate change impacts, hazardous (drought), and sectors use of
water resource (e.g. households water use, irrigation water use, cooling water, hydropower production etc.).

**Reported data**
-----------------

*Note*: In the following section you will find some selected tables with information concerning the reporting in the period from 1992 to 2012 (if years are available). Some of the comments and questions refer to other tables (background tables). 
You will find these background tables as well as an overview on all background tables in your country folder.

.. include:: /S09-Water-Quantity/comments/ZZ.txt
   :start-after: start-text1-placeholder
   :end-before: end-text1-placeholder
   

*Note*: **Water Balance** is a general category that is comprised by 50 different parameters that are describing all aspects of the hydrological water balance in a reference area (country, RBD, SU etc.) including water transfers by human activity. Examples of water balance parameters are areal precipitation, actual evapotranspiration, but also water imports and exports in the reference area etc.

**Water Availability and Abstractions (WA & A)** is a category that is interested in the abstractions of fresh and non fresh water that are performed in a certain reference area, and also in the availability of water resources in this area. These abstractions are divided according to the economic sector, the public or the self supply system responsible for the abstraction and the source of abstraction (ground water or surface water). In total 60 different parameters are listed in this category.

**Water use** is the general category that water actually consumed is recorded. Water quantities are divided by economic sector (households, agriculture, industries, etc), type of water (fresh water or reused), and the supply system (public or self supply). The main difference from the previous category (WA & A) is that not all abstractions end up as consumed water due to the returns to the environment. This category contains 71 different parameters.

**Percentage (%) of reporting parameters** is calculated by the fraction of the total number of the reported areal parameters (in any temporal scale) to the total number of areal parameters in all categories (50+60+71=181 parameters) multiplied by 100. For instance if a MS has reported 35 different parameters (sum of all categories) then this percentage is 35/181X100=19.33%=>19%
   
   
.. include:: /S09-Water-Quantity/tables/S09_ZZ_table51.txt   
   
.. include:: /S09-Water-Quantity/comments/ZZ.txt
   :start-after: start-text2-placeholder
   :end-before: end-text2-placeholder   


.. include:: /S09-Water-Quantity/tables/S09_ZZ_table52.txt
	
*Note:* Both point and areal data are requested in the WISE-SoE water quantity dataflow.
Member states are requested to report in the smallest available temporal and spatial scales in order to identify issues concerning water shortage or overconsumption. The most adequate scales for areal data are SU or RBD spatial scale and monthly or seasonal temporal scale. RBD scale is requested further more in the WFD reporting. For example: If water abstraction is a significant pressure for a specific RBD in a country with adequate water resources and data are reported only at country level, this issue will probably not be identified. Additionally, one issue of great importance is the reliability of point data's coordinates. Please double check if the reported coordinates of point data, such as gauging stations, are in proximity with a water body (river, lake, etc.). 
Finally the basic parameters needed for Water Accounts are presented in supportive Table S09_Table5B2, so MSs are encouraged to report water accounts under the format of these tables.


*Questions regarding the reporting on water quantity*
-----------------------------------------------------

.. include:: /S09-Water-Quantity/questions/ZZ.txt

