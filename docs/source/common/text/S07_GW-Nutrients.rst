
Groundwater
***********

**Reporting obligation**

Data on groundwater bodies are collected through the WISE-SoE data collection process (`Groundwater quality EWN-3 <http://rod.eionet.europa.eu/obligations/30>`_).
Reporting under this obligation is used for EEA Core set of indicators (see EEA products).

The data requested on groundwater include the physical characteristics of the groundwater bodies,
proxy pressures on the groundwater area, location and parameters of groundwater monitoring stations including their relation
to groundwater body as well as chemical quality data on nutrients and organic matter, and hazardous substances in groundwater.
Determinands which should be reported under this data flow are shown in the following figure.

.. image:: /common/images/determinands_gw.jpg
   :width: 50%
   
Closely related to the SoE reporting on groundwater water quality is the reporting under the WFD (`WFD River Basin Management Plan including
programme of measures <http://rod.eionet.europa.eu/obligations/521>`_ ).
The `new WFD reporting guidance 2016 <http://icm.eionet.europa.eu/schemas/dir200060ec/resources2014>`_ for the 2nd reporting includes concrete references to SoE reporting.
The information reported by EEA countries can only be interpreted reasonably when streamlined with the WFD reporting.
Combining water quality results with information on chemical status and potential, as foreseen in the WFD reporting guidance,
are only possible if the WFD water body is linked with SoE stations which provide long time series on relevant determinants.





Groundwater - Nutrients, Organic Matter and General Physico-Chemical Determinands
=======================================================================================

**Use of data by the EEA**
--------------------------

The information will be used to formulate indicators that will be used to assess state and trend of the determinands and monitor progress with
European policy objectives.

**EEA products**

* `Waterbase - Groundwater <http://www.eea.europa.eu/data-and-maps/data/waterbase-groundwater-10>`_

* EEA indicators
	*	`Nutrients in Freshwater (CSI 020) <http://www.eea.europa.eu/data-and-maps/indicators/nutrients-in-freshwater/nutrients-in-freshwater-assessment-published-6>`_

* EEA reports
	*	`EEA 2010: Freshwater quality - SOER 2010 thematic assessment. <http://www.eea.europa.eu/soer/europe/freshwater-quality>`_
	*	`EEA ETC/ICM 2010: Freshwater Eutrophication Assessment - Background Report for EEA European Environment State and Outlook Report 2010. <http://icm.eionet.europa.eu/docs/Freshwater_eutrophication_background_report_29_Nov_2010_final4.pdf>`_
	*	`EEA, 2012Water resources in Europe in the context of vulnerability <http://www.eea.europa.eu/publications/water-resources-and-vulnerability>`_
	*	`EEA, 2012 European waters - assessment of status and pressures, EEA Report No 8/2012 <http://www.eea.europa.eu/publications/european-waters-assessment-2012>`_
	*	`EEA ETC/ICM 2012: Ecological and chemical status and pressures. Thematic assessment for EEA Water 2012 <http://icm.eionet.europa.eu/ETC_Reports/EcoChemStatusPressInEurWaters_201211>`_

* EEA maps
	*	`Nitrates in groundwater by countries and WFD gw bodies <http://www.eea.europa.eu/data-and-maps/explore-interactive-maps/nitrates-in-groundwater-by-countries>`_
	*	`Nitrites in groundwater by countries and WFD gw bodies <http://www.eea.europa.eu/data-and-maps/explore-interactive-maps/nitrites-in-groundwater-by-countries>`_
	*	`Ammonium in groundwater by countries and WFD gw bodies <http://www.eea.europa.eu/data-and-maps/explore-interactive-maps/ammonium-in-groundwater-by-countries>`_
	*	`SoE monitoring stations <http://www.eea.europa.eu/data-and-maps/explore-interactive-maps/overview-of-soe-monitoring-stations>`_

* EEA datasets
	*	`GIS layer on WFD groundwater bodies <http://www.eea.europa.eu/data-and-maps/data/wise-groundwater>`_





**Reported data**
-----------------

*Note*: In the following section you will find some selected tables with information concerning the reporting in the period from 1992 to 2012 (if years are available). Some of the comments and questions refer to other tables (background tables). 
You will find these background tables as well as an overview on all background tables in your country folder.

**Determinands**

Table 4.1 and Table 4.2  provide an overview by determinands (highest priority nutrients) of the number of groundwater stations /
groundwater bodies per year.

.. S07

.. include:: /S07-GW-nutrients/comments/ZZ.txt
   :start-after: start-text1-placeholder
   :end-before: end-text1-placeholder
 
.. include:: /S07-GW-nutrients/tables/S07_ZZ_table41.txt

.. include:: /S07-GW-nutrients/tables/S07_ZZ_table42.txt

*Note*: In the current data set the reporting of high priority determinands has stopped or nutrients have been reported as aggregated data only.
The aim is to ensure that the high priority determinands (preferably disaggregated data) have as complete coverage as possible.

**Station coverage**

Table 4.3 shows the number of groundwater stations by River Basin Districts which reported on highest priority nutrients.


.. include:: /S07-GW-nutrients/comments/ZZ.txt
   :start-after: start-text2-placeholder
   :end-before: end-text2-placeholder

.. include:: /S07-GW-nutrients/tables/S07_ZZ_table43.txt
	
*Note*: One aspect of the quality fact sheets is to improve the spatial coverage and ensure that stations are reported for all RBDs.

**Time series**

The trend evaluation in EEA water quality indicators (CSI20) is based on stations with data from the period 1992-2012 or 2000/2003-2012.

.. include:: /S07-GW-nutrients/comments/ZZ.txt
   :start-after: start-text3-placeholder
   :end-before: end-text3-placeholder

*Note*: EEA water quality indicators are used for trend assessments based on consistent time series with some gap filling. For a single country consistent time series are established for the defined period (e.g. 1992-2012; or 2000-2012) with some gap filling (e.g. up to 3 years)  and only stations with values for all years in the defined period are used. This ensures that any trend is because of change in the observations and not in the stations included. Sometimes there is a stop in reporting stations in some years; or stations cannot be clearly identified, e.g. because of missing or changing station names.  EEA wants to clarify these issues and improve the consistency of time series for high priority nutrients.


   
*Questions regarding the reporting on general water quality in groundwater*
---------------------------------------------------------------------------

.. include:: /S07-GW-nutrients/questions/ZZ.txt



.. footnotes-placeholder

.. [#S07f1]    The lists of "preferred" substances are based on legislation, spatial and temporal availability.
      Preferred substances are also covered by Hazardous substances report regularly.         
         
