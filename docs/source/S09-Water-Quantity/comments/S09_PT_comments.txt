.. start-text1-placeholder



Portugal has reported 4 parameters in the "Water Availability & Abstractions" category, and 1-3 parameters in the "Water Balance" category in RBD spatial scale. In total 
3-4% of the number of total parameters have been reported in RBD spatial scale for 10 RBDs.

.. end-text1-placeholder

.. start-text2-placeholder



Portugal has reported 54 timeseries in annual temporal scale. Also 9 timeseries are in seasonal temporal scale.
The reported period for all timeseries cover the year 2006. The timeseries by category are shown in Table 5.2.

.. end-text2-placeholder



