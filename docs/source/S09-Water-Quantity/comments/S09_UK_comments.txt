
.. start-text1-placeholder



United Kingdom has reported 13-16 parameters in the "Water Availability & Abstractions" category, in RBD spatial scale for 11 RBDs. In total 
7%-9% of the number of parameters have been reported in RBD spatial scale.

.. end-text1-placeholder

.. start-text2-placeholder



United Kingdom has reported 168 timeseries in annual temporal scale. The reported period for all timeseries cover the year 2001. The timeseries by category are shown in Table 5.2.

.. end-text2-placeholder
