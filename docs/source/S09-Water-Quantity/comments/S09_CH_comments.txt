.. start-text1-placeholder



Switzerland has reported 34 parameters in the "Water Availability & Abstractions" category, 24 parameters in the "Water Balance" category and 55 parameters in the c category in country spatial scale. In total 
62% of the number of parameters have been reported in country spatial scale. In SU spatial scale has reported 1 parameter in the category of "Water Availability and Abstractions" and 3-15 parameters in the "Water Balance" category for 15 Sub Units.
Also has reported 1-28 parameters in "Water Availability & Abstractions", 2-8  parameters  in "Water Balance" and 48-52 parameters in "Water Use" categories for 5 RBDs.

.. end-text1-placeholder

.. start-text2-placeholder



Switzerland has reported 799 timeseries in annual temporal scale. Also 153 timeseries in monthly and 81 timeseries in daily temporal scale.
The reported period for some "Water Balance" timeseries cover the years 1901 since 2012, the longest reporting period in SoE water quantity data flow. The other timeseries cover the period 2006 since 2012. Finally it has reported 91 LTAA values from streamflow stations and groundwater level in wells.The timeseries by category are shown in Table 5.2.

.. end-text2-placeholder
