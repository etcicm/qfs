.. start-text1-placeholder

Macedonia (FYR) has reported 17 parameters in the "Water Availability & Abstractions" category, 3 parameters for the "Water Balance" category and 34 parameters in the "Water Use" category in country spatial scale. In total 
30% of the number of total parameters have been reported in country spatial scale. In NUTS1, NUTS2 and NUTS3 spatial scales has reported 4-14 parameters in the category of "Water Availability & Abstractions" and 17-27 parameters for the category of "Water Use", 
for 1 NUTS1, 1 NUTS2 and 8 NUTS3 spatial scale units.

.. end-text1-placeholder

.. start-text2-placeholder



Macedonia (FUR) has reported 476 timeseries in annual temporal scale. Also 54 timeseries in monthly and 10 timeseries in daily temporal scale.
The reported period for all timeseries cover the years 2005 since 2012.Finally it has reported 9 LTAA values from streamflow stations and groundwater level in wells. The timeseries by category are shown in Table 5.2.

.. end-text2-placeholder


