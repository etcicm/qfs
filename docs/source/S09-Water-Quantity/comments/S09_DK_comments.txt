.. start-text1-placeholder



Denmark has reported 19 parameters in the "Water Availability & Abstractions" category, 19 parameters in the "Water Use" category and 4 parameters in the "water balance" category in country spatial scale. In total 
23% of the number of total parameters have been reported in country spatial scale. In RBD spatial scale has reported 6 parameters in the category of water balance for one RBD (Zealand).
	
.. end-text1-placeholder

.. start-text2-placeholder



Denmark has reported 807 timeseries in annual temporal scale. Also 200 timeseries are in daily temporal scale.
The reported period for all timeseries cover the years 2000 since 2012. The timeseries by category are shown in Table 5.2.

.. end-text2-placeholder
