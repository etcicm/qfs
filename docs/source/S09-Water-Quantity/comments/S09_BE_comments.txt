.. start-text1-placeholder



Belgium has reported 39 parameters in the "Water Availability & Abstractions" category, 16 parameters in the "Water Balance" category and 42 parameters in the "Water Use" category in NUTS1 spatial scale. In this scale 54% of the total number of parameters has been reported. 
Also Bergium reported water quantity data for 7 RBDs with parameter coverage ranging from 4% to 52 %.

.. end-text1-placeholder

.. start-text2-placeholder



Belgium has reported 459 timeseries in annual temporal scale. Also 214 timeseries in monthly and 83 timeseries in daily temporal scale.In total 756 timeseries have been reported.
The reported period for all timeseries cover the years 2007 since 2012. The timeseries by category are shown in Table 5.2.

.. end-text2-placeholder




