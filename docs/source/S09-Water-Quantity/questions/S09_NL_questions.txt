
**Clarifying questions on data in current database**

#. Some outliers have been identified in stations data and a logical violation rule on water abstraction data (http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/wise-soe-reporting-2013/validation-questions/country-folders/netherlands/water-quantity/nl-outliers). Could it be possible to identify the source of error and report back the updated data in the next reporting cycle?

**Improving coverage of determinands, temporal and spatial coverage**  
 
#.  Could it be possible to expand the temporal coverage of the available data (2006, Table 5.2) for years before 2006 and fulfil the gaps? 

**Links/references** 

Please provide further references to

#.  National water quantity reports
#.  National water quantity indicators
#.  National water quantity data sets - databases