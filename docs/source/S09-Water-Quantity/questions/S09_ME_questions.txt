**Clarifying questions on data in current database**

#. Not any QA/QC has been detected under Seo Water Quantity Data flow.

**Improving coverage of determinands, temporal and spatial coverage**  

#. Water use, water balance and water abstraction categories are essential sets of parameters for assessing the European overview on water quantity. Could it be possible to be reported in the next reporting cycle?

#.  Could it be possible to expand the temporal coverage of the available data (2011 Table 5.2) for previous and latter years? 

**Links/references** 

Please provide further references to

#.  National water quantity reports
#.  National water quantity indicators
#.  National water quantity data sets - databases