
**Clarifying questions on data in current database**

#. Some outliers have been identified in stations coordinates. Also outliers detected on water abstraction, water use and water balance data (http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/wise-soe-reporting-2013/validation-questions/country-folders/lithuania/water-quantity/lt-outliers). Could it be possible to identify the source of error and report back the updated data in the next reporting cycle?

**Improving coverage of determinands, temporal and spatial coverage**  


#. Could it be possible to expand the temporal coverage of the available data (2007-2012, Table 5.2) for previous years?

**Links/references** 

Please provide further references to

#.  National water quantity reports
#.  National water quantity indicators
#.  National water quantity data sets - databases