**Clarifying questions on data in current database**

#. Some outliers and logical rule violations have been identified in data (http://forum.eionet.europa.eu/nrc-eionet-freshwater/library/wise-soe-reporting-2013/validation-questions/country-folders/switzerland/water-quantity/ch-outliers). Could it be possible to identify the source of error and report back the updated data in the next reporting cycle?

**Improving coverage of determinands, temporal and spatial coverage**  
 
#. Could it be possible to expand the temporal coverage of the available data (2006-2012, Table 5.2) for previous years?

**Links/references** 

Please provide further references to

#.  National water quantity reports
#.  National water quantity indicators
#.  National water quantity data sets - databases