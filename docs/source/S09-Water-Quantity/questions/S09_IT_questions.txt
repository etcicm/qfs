**Improving coverage of determinands, temporal and spatial coverage**  
 
#. Water quantity data is essential for providing European overview on water quantity related assessment. Italy could not be included in such assessment due to the lack of respective data reported. Could it be possible to establish the data reporting under SoE Water Quantity data in the future? 

**Links/references** 

Please provide further references to

#.  National water quantity reports
#.  National water quantity indicators
#.  National water quantity data sets - databases